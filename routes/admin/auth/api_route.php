<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Authorized Group
$api->version('v1', [
    'middleware' => [
        'cors2',
        'trimInput',
        'verifySecret',
        'authorize'
    ]
], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\V1\CMS\Controllers'], function ($api) {

        $api->options('/{any:.*}', function () {
            return response(['status' => 'success'])
                ->header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                ->header('Access-Control-Allow-Headers', 'Authorization, Content-Type, Origin');
        });


        $api->get('/', function () {
            return ['api-status' => 'CMS API Ok!'];
        });

        // News
        require __DIR__ . '/news.php';

        // Users
        require __DIR__ . '/user.php';

        // Support
        require __DIR__ . '/support.php';

        // Info
        require __DIR__ . '/info.php';

        // Notify
        require __DIR__ . '/notify.php';

        // Import
        require __DIR__ . '/import.php';

        // Role
        require __DIR__ . '/role.php';

        // Permissions
        require __DIR__ . '/permission.php';

        // Permission Group
        require __DIR__ . '/permission_group.php';

        // Departments
        require __DIR__ . '/departments.php';

        // Issue
        require __DIR__ . '/issue.php';

        // Module Category
        require __DIR__ . '/module_category.php';

        // Discuss
        require __DIR__ . '/discuss.php';

        // Discuss
        require __DIR__ . '/module.php';

        // Files
        require __DIR__ . '/file.php';

        // FOLDERS
        require __DIR__ . '/folders.php';

        // Report
        require __DIR__ . '/report.php';

        // User Log
        require __DIR__ . '/user_log.php';

        // Log Word
        require __DIR__ . '/log_word.php';

        // Static Drive
        require __DIR__ . '/static_drive.php';

        // Product
        require __DIR__ . '/product.php';

        // Color
        require __DIR__ . '/color.php';

        // Size
        require __DIR__ . '/size.php';

        // mes
        require __DIR__ . '/mes.php';

    });
});

