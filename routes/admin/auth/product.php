<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/20/2019
 * Time: 3:48 PM
 */

$api->get('/products', [
    'action' => '',
    'uses'   => 'ProductAppController@search',
]);

$api->get('/products/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ProductAppController@detail',
]);

$api->post('/products', [
    'action' => '',
    'uses'   => 'ProductAppController@create',
]);

$api->put('/products/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ProductAppController@update',
]);

