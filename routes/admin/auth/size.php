<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:42 PM
 */

$api->get('/site', [
    'action' => '',
    'uses'   => 'SizeController@search',
]);

$api->get('/site/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'SizeController@detail',
]);

$api->post('/site', [
    'action' => '',
    'uses'   => 'SizeController@store',
]);

$api->put('/site/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'SizeController@update',
]);

