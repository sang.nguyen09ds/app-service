<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api->get('/departments', [
    'action' => 'VIEW-DEPARTMENTS',
    'uses'   => 'DepartmenrtsController@search',
]);

$api->get('/departments/{id:[0-9]+}', [
    'action' => 'VIEW-DEPARTMENTS',
    'uses'   => 'DepartmenrtsController@detail',
]);

$api->post('/departments', [
    'action' => 'CREATE-DEPARTMENTS',
    'uses'   => 'DepartmenrtsController@create',
]);

$api->put('/departments/{id:[0-9]+}', [
    'action' => 'UPDATE-DEPARTMENTS',
    'uses'   => 'DepartmenrtsController@update',
]);

$api->delete('/departments/{id:[0-9]+}', [
    'action' => 'DELETE-DEPARTMENTS',
    'uses'   => 'DepartmenrtsController@delete',
]);