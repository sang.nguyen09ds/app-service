<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:41 PM
 */


$api->get('/color', [
    'action' => '',
    'uses'   => 'ColorController@search',
]);

$api->get('/color/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ColorController@detail',
]);

$api->post('/color', [
    'action' => '',
    'uses'   => 'ColorController@create',
]);

$api->put('/color/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ColorController@update',
]);
