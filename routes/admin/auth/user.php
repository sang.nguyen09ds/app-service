<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api->get('/users', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@search',
]);
$api->get('/users-k-drive', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@searchKDrive',
]);

$api->get('/users/{id:[0-9]+}', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@view',
]);
/*$api->get('/users/profile', [
    'action' => 'VIEW-USER',
    'uses' => 'UserController@searchProfile',
]);*/

$api->get('/users/profile', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@viewProfile',
]);

$api->get('/users/export', [
    'action' => 'VIEW-USER-EXPORT',
    'uses'   => 'UserController@exportUser',
]);

$api->get('/users/profile/{id:[0-9]+}', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@viewProfileUser',
]);

$api->put('/users/change-password', [
    'action' => 'UPDATE-USER-PASSWORD',
    'uses'   => 'UserController@changePassword',
]);


$api->get('/users/info', [
    'action' => 'VIEW-USER',
    'uses'   => 'UserController@getInfo',
]);

$api->post('/users', [
    'action' => 'UPDATE-USER',
    'uses'   => 'UserController@create',
]);

$api->post('/users/profile', [
    'action' => 'UPDATE-USER',
    'uses'   => 'UserController@updateProfile',
]);

$api->put('/users/{id:[0-9]+}', [
    'action' => 'UPDATE-USER',
    'uses'   => 'UserController@update',
]);


$api->delete('/users/{id:[0-9]+}', [
    'action' => 'DELETE-USER',
    'uses'   => 'UserController@delete',
]);

$api->put('/users/{id:[0-9]+}/active', [
    'action' => 'UPDATE-USER',
    'uses'   => 'UserController@active',
]);