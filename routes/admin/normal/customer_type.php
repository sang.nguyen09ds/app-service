<?php
/**
 * User: Administrator
 * Date: 01/01/2019
 * Time: 10:31 PM
 */

$api->get('/customer_types', [
    'action' => 'VIEW-CUSTOMER-TYPE',
    'uses'   => 'CustomerTypeController@search',
]);

$api->get('/customer_types/{id:[0-9]+}', [
    'action' => 'VIEW-CUSTOMER-TYPE',
    'uses'   => 'CustomerTypeController@detail',
]);

$api->post('/customer_types', [
    'action' => 'CREATE-CUSTOMER-TYPE',
    'uses'   => 'CustomerTypeController@create',
]);

$api->put('/customer_types/{id:[0-9]+}', [
    'action' => 'UPDATE-CUSTOMER-TYPE',
    'uses'   => 'CustomerTypeController@update',
]);

$api->delete('/customer_types/{id:[0-9]+}', [
    'action' => 'UPDATE-CUSTOMER-TYPE',
    'uses'   => 'CustomerTypeController@delete',
]);
