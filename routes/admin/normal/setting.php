<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$api->get('/{country_id:[0-9]+}/cities', [
    'action' => '',
    'uses' => 'SettingController@getCity',
]);

$api->get('/{city_id:[0-9]+}/districts', [
    'action' => '',
    'uses' => 'SettingController@getDistrict',
]);

$api->get('{district_id:[0-9]+}/wards', [
    'action' => '',
    'uses' => 'SettingController@getWard',
]);