<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
function pdf()
{
    $path = storage_path() . '/customer/card/';
    $data = file_get_contents($path . "front.png");
    $dataFr = 'data:image/jpg;base64,' . base64_encode($data);
    $data = file_get_contents($path . "back.png");
    $dataBk = 'data:image/jpg;base64,' . base64_encode($data);

    $qrData = "data:image/png;base64," . base64_encode(\QR::format('png')
            ->size(200)
            ->errorCorrection('H')
            ->generate("12345678"));

    $view = view('customer/card/customer_card', [
        'front'      => $dataFr,
        'back'       => $dataBk,
        'card_from'  => '01/09',
        'card_name'  => 'NGUYEN VAN SANG',
        'card_sccid' => '123456',
        'qr_code'    => $qrData,
    ]);
    echo $view;
    die;
}

// Normal Group
$api->version('v1', ['middleware' => ['cors']], function ($api) {
    $api->group(['prefix' => 'v0', 'namespace' => 'App\V1\CMS\Controllers'], function ($api) {

        $api->get('/', function () {
            return ['api-status' => 'Nomal API status: Ok!'];
        });

        $api->get('/img/{img}', function ($img) {
            $img = str_replace(",", "/", $img);
            public_path() . "/" . $img;

            $extension = pathinfo($img, PATHINFO_EXTENSION);
            if ($extension == 'jpg' || $extension == 'png') {

                $fp = fopen(public_path() . "/" . $img, 'rb');

                header("Content-Type: image/png");
                header("Content-Length: " . filesize(public_path() . "/" . $img));
                fpassthru($fp);
            }
            if ($extension == 'pdf') {
                $filePdf = public_path() . "/" . $img;
                $file = $filePdf;
                $filename = 'filename.pdf';
                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename="' . $filename . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
                // doawload file
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $filename . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);

//                header('Content-type: application/pdf');
//                header('Content-Disposition: attachment; filename="' . $filename . '"');
//                header('Content-Transfer-Encoding: binary');
//                header('Accept-Ranges: bytes');
//                @readfile($file);
            }
            $arrextension=[
                'docx' =>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'xlsx' =>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'pptx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'txt' =>'application/octet-stream',
                'doc'=>'application/msword',
                'xls' =>'application/vnd.ms-excel',
            ];
            foreach ($arrextension as $key => $value)
            {
               if($extension === $key)
               {
                   $filePdf = public_path() . "/" . $img;
                   $file = $filePdf;
                   header('Content-Type:'+ $value);
                   //application/vnd.openxmlformats-officedocument.wordprocessingml.document
                   header('Content-Disposition: attachment;filename="' . $file . '"');
                   readfile($file);
                   exit;
               }
            }
//            if ($extension == 'docx') {
//                $filePdf = public_path() . "/" . $img;
//                $file = $filePdf;
//                $filename = 'filename.doc';
//                header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
//                header('Content-Disposition: attachment;filename="' . $file . '"');
//                readfile($file);
//            }
//            if ($extension == 'xlsx' ||$extension=="pptx") {
//                $filePdf = public_path() . "/" . $img;
//                $file = $filePdf;
//                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//               header('Content-Disposition: attachment;filename="' . $file . '"');
//                readfile("$file");
//            }
//            if ($extension == 'txt') {
//                $filePdf = public_path() . "/" . $img;
//                $file = $filePdf;
//                header('Content-Type: application/octet-stream');
//                header('Content-Disposition: attachment;filename="' . $file . '"');
//                readfile("$file");
//            }
//            if ($extension == 'doc') {
//                $filePdf = public_path() . "/" . $img;
//                $file = $filePdf;
//                header('Content-Type: application/msword');
//                header('Content-Disposition: attachment;filename="' . $file . '"');
//                readfile("$file");
//            }
//            if ($extension == 'xls') {
//                $filePdf = public_path() . "/" . $img;
//                $file = $filePdf;
//                header('Content-Type: application/vnd.ms-excel');
//                header('Content-Disposition: attachment;filename="' . $file . '"');
//                readfile("$file");
//            }
            //exit;
        });

        // News
        require __DIR__ . '/news.php';

        // Setting
        require __DIR__ . '/setting.php';

        // Plant Group
        require __DIR__ . '/plant_group.php';

        // Plant
        require __DIR__ . '/plant.php';

        // Land
        require __DIR__ . '/land.php';

        // NPK Manure
        require __DIR__ . '/npk_manure.php';

        // Customer
        require __DIR__ . '/customer.php';

        // Customer Type
        require __DIR__ . '/customer_type.php';

        // Users
        require __DIR__ . '/users.php';

    });
});

