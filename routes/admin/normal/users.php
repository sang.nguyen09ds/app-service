<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 7/2/2019
 * Time: 9:04 PM
 */
$api->get('/reset-password', [
    'action' => '',
    'uses'   => 'UserController@resetPassword',
]);

$api->get('/verifyCode', [
    'action' => '',
    'uses'   => 'UserController@verifyCode',
]);