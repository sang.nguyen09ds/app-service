<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version() . "- APP -API";
});
// Admin Authorization
require_once __DIR__ . '/auth_admin.php';

// Customer Authorization
require_once __DIR__ . '/auth_web.php';

$api = app('Dingo\Api\Routing\Router');

// Normal API
require __DIR__ . '/admin/normal/api_route.php';
// Authorize API
require __DIR__ . '/admin/auth/api_route.php';

// Normal API
require __DIR__ . '/web/normal/api_route.php';
// Authorize API
require __DIR__ . '/web/auth/api_route.php';
