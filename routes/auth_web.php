<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Authorization
$router->group(['prefix' => 'home', 'namespace' => 'Auth', 'middleware' => ['cors2', 'trimInput', 'auth_web']], function ($router) {
    // Auth
    $router->post('/login', "LoginController@authenticate");
    $router->get('/token', "LoginController@checkToken");
    $router->get('/logout', "LoginController@logout");
    $router->post('/register', "LoginController@register");
    $router->get('/forget-password', "LoginController@forgetPassword");
    $router->post('/reset-password', "LoginController@resetPassword");

    // Normal API
    $router->get('/customer-info', "LoginController@currentCustomerInfo");
});