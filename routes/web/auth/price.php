<?php
/**
 * User: Dai Ho
 * Date: 01/01/2019
 * Time: 21:18
 */

$api->get('/prices', [
    'uses' => 'PriceController@search',
]);

$api->get('/prices/{id:[0-9]+}', [
    'uses' => 'PriceController@detail',
]);

////////// Price Detail //////////
$api->get('/prices/details', [
    'action' => 'VIEW-PRICE',
    'uses' => 'PriceController@priceDetailList',
]);

$api->get('/prices/{code}/details', [
    'action' => 'VIEW-PRICE',
    'uses' => 'PriceController@priceDetail',
]);