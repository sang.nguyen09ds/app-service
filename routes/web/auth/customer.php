<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api->get('/customers', [
    'action' => 'VIEW-CUSTOMER',
    'uses'   => 'CustomerController@search',
]);

$api->get('/customers/{code}/detail', [
    'action' => 'VIEW-CUSTOMER',
    'uses'   => 'CustomerController@detail',
]);

$api->get('/customers/info', [
    'action' => '',
    'uses'   => 'CustomerController@info',
]);

$api->put('/customers/info', [
    'action' => '',
    'uses'   => 'CustomerController@updateProfile',
]);