<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api->get('/orders', [
    'uses' => 'OrderController@search',
]);

$api->get('/orders/{code}', [
    'uses' => 'OrderController@view',
]);

$api->post('/orders', [
    'action' => 'CREATE-ORDER',
    'uses'   => 'OrderController@create',
]);

$api->put('/orders/{id:[0-9]+}', [
    'action' => 'UPDATE-ORDER',
    'uses'   => 'OrderController@update',
]);


$api->delete('/orders/{id:[0-9]+}', [
    'action' => 'DELETE-ORDER',
    'uses'   => 'OrderController@delete',
]);