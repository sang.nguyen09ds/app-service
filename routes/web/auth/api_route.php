<?php
/**
 * User: Administrator
 * Date: 29/12/2018
 * Time: 10:34 AM
 */

// Authorized Group
$api->version('v1', [
    'middleware' => [
        'cors2',
        'trimInput',
        'auth_web',
        'webVerifySecret',
        'webAuthorize'
    ]
], function ($api) {
    $api->group(['prefix' => 'home-v1', 'namespace' => 'App\V1\Home\Controllers'], function ($api) {

        $api->options('/{any:.*}', function () {
            return response(['status' => 'success'])
                ->header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                ->header('Access-Control-Allow-Headers', 'Authorization, Content-Type, Origin');
        });

        $api->get('/', function () {
            return ['api-status' => 'Web API status: Ok!'];
        });

        // Customer
        require __DIR__ . '/customer.php';

        // Order
        require __DIR__ . '/order.php';

        // Order
        require __DIR__ . '/cart.php';

        // User
        require __DIR__ . '/user.php';

        // Warehouse
        require __DIR__ . '/warehouse.php';

        // Price
        require __DIR__ . '/price.php';

    });
});