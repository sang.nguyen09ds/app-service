<?php
/**
 * User: Administrator
 * Date: 29/12/2018
 * Time: 10:34 AM
 */


// Normal Group
$api->version('v1', ['middleware' => ['cors']], function ($api) {
    $api->group(['prefix' => 'home-v0', 'namespace' => 'App\V1\Home\Controllers'], function ($api) {

        $api->options('/{any:.*}', function () {
            return response(['status' => 'success'])
                ->header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                ->header('Access-Control-Allow-Headers', 'Authorization, Content-Type, Origin');
        });
        $api->get('/', function () {
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            // set font
            $pdf->SetFont('dejavusans', '', 10);
            // add a page
            $pdf->AddPage();

            $order = \App\Order::find(1);
            $html = view("customer.order_receipt", ['order' => $order]);
            //echo $html;die;
            // output the HTML content
            $pdf->writeHTML($html);
            //$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,H', 80, 210, 50, 50, null, 'N');
            //$pdf->Text(80, 205, 'QRCODE H - COLORED');
            // reset pointer to the last page
            //$pdf->lastPage();
            //Close and output PDF document
            $pdf->Output('example_006.pdf', 'I');

        });

        // Customer
        require __DIR__ . '/customer.php';

        // Product
        require __DIR__ . '/product.php';

        // Category
        require __DIR__ . '/category.php';

        // Image
        require __DIR__ . '/image.php';

        // Module Test
        require __DIR__ . '/module_test.php';
    });
});

