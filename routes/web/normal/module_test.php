<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api->get('/module', [
    'uses' => 'ModuleTestController@search',
]);

$api->get('/module/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ModuleTestController@detail',
]);

$api->post('/module', [
    'action' => '',
    'uses'   => 'ModuleTestController@create',
]);

$api->put('/module/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ModuleTestController@update',
]);

$api->delete('/module/{id:[0-9]+}', [
    'action' => '',
    'uses'   => 'ModuleTestController@delete',
]);