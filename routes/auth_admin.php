<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Authorization
$router->group(['prefix' => 'auth', 'namespace' => 'Auth', 'middleware' => ['cors2', 'trimInput', 'auth_admin']], function ($router) {
    // Auth
    $router->post('/login', "AuthController@authenticate");
    $router->post('/user-login', "AuthController@userLogin");
    $router->post('/user-register', "AuthController@userRegister");
    $router->get('/token', "AuthController@checkToken");
    $router->get('/logout', "AuthController@logout");
    $router->get('/forget-password', "AuthController@forgetPassword");
    $router->post('/reset-password', "AuthController@resetPassword");

    // Normal API
    $router->get('/user-info', "AuthController@currentUserInfo");
});