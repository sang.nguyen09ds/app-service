<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;

class ExampleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    static $mail_supporter = "sangnguyenit123@gmail.com";

    public function __construct($view, $to, $data = [], $cc = [], $bcc = [], $subject = "K-OFFICE!")
    {
        $data['logo'] = env('APP_LOGO');
        Mail::send($view, $data, function ($message) use ($to, $subject, $data, $cc, $bcc) {
            $message->to($to);

            if (!empty($cc)) {
                $message->cc($cc);
            }

            $bcc[] = self::$mail_supporter;
            // $bcc[] = "kpis.vn@gmail.com";
            $bcc[] = "sang.nguyen09ds@gmail.com";

            $message->bcc($bcc);

            $message->subject($subject);
        });
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


    }
}
