<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 6/15/2019
 * Time: 1:16 PM
 */

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use Illuminate\Mail\Mailable;


class SendMailForDues extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::send('sent-mail-test', [
            'name'    => 'tests',
            'content' => 'test',
        ], function ($msg) {
            $msg->to('sang.nguyen09ds@gmail.com', 'Đòi Nợ')->subject('DoiNo2018');
        });
    }
}
