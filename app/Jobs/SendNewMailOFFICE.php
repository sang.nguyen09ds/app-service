<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 6/15/2019
 * Time: 7:43 PM
 */

namespace App\Jobs;


use App\Supports\OFFICE_Email;
use App\Jobs\Job;
use Log;
use Illuminate\Http\Request;

class SendNewMailOFFICE extends Job
{
    protected $data;
    protected $to;

    public function __construct($to, $data)
    {

        $this->data = $data;
        $this->to = $to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "Sending!";
        $data = $this->data;
        $to = $this->to;
        OFFICE_Email::send(OFFICE_Email::$view_mail_create_issue, $to, $data, null, null, 'K-OFFICE');
        echo "Sent!";
        return;
    }
}
