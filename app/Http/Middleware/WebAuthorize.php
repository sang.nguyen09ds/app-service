<?php

namespace App\Http\Middleware;

use App\RolePermission;
use App\OFFICE;
use App\Supports\Message;
use App\Customer;
use App\User;
use Closure;
use Dingo\Api\Routing\Router;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\CUS;

class WebAuthorize
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var
     */
    protected $customerInfo;

    /**
     * @var
     */
    protected $permissions;


    /**
     * Authorize constructor.
     */
    public function __construct(Router $router)
    {
        if (!OFFICE::allowRemote()) {
            throw new AccessDeniedHttpException(Message::get('remote_denied'));
        }

        $this->router = $router;
        $customerId = CUS::getCurrentCustomerId();
        if (empty($customerId)) {
            throw new \Exception(Message::get("unauthorized"));
        }

        $customerType = CUS::getCurrentCustomerType();
        if ($customerType == USER_TYPE_SELLER) {
            $customer = User::find($customerId);
        } else {
            $customer = Customer::find($customerId);
        }

        if (empty($customer)) {
            throw new \Exception(Message::get("unauthorized"));
        }

        $this->customerInfo = $customer;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permissions = [];
        if (!empty($this->customerInfo->role_id)) {
            $currentPermissions = RolePermission::with(['permission'])
                ->where('role_id', $this->customerInfo->role_id)->get()->toArray();
            $permissions = array_pluck($currentPermissions, null, 'permission.code');
        }

        $action = array_get($this->router->getCurrentRoute()->getAction(), 'action', null);

        if (!$action) {
            return $next($request);
            //throw new AccessDeniedHttpException('Permission denied!');
        }

        if (empty($permissions[$action])) {
            throw new AccessDeniedHttpException(Message::get("no_permission"));
        }

        return $next($request);
    }

}
