<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 27/12/2018
 * Time: 10:17 PM
 */

namespace App\Http\Middleware;


use Illuminate\Support\Facades\Config;

class AuthWeb
{
    public function handle($request, \Closure $next)
    {
        Config::set('jwt.user' , 'App\Customer');
        Config::set('auth.providers.users.model', \App\Customer::class);

        return $next($request);
    }
}