<?php
/**
 * User: Dai Ho
 * Date: 22-Mar-17
 * Time: 23:43
 */

namespace App\Http\Transformers;

use App\Supports\OFFICE_Error;
use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserInfoTransformer
 * @package App\Http\Transformers
 */
class UserInfoTransformer extends TransformerAbstract {
    public function transform(User $user) {
        try {
            return [
                'id'         => $user->id,
                'user'       => $user->user,
                'email'      => $user->email,
                'code'       => strtoupper($user->code),
                'first_name' => object_get($user, "profile.first_name", null),
                'last_name'  => object_get($user, "profile.last_name", null),
                'short_name' => object_get($user, "profile.short_name", null),
                'full_name'  => object_get($user, "profile.full_name", null),
                'address'    => object_get($user, "profile.address", null),
                'phone'      => object_get($user, "profile.phone", null),
                'birthday'   => object_get($user, 'profile.birthday', null),
                'genre'      => object_get($user, "profile.genre", "O"),
                'genre_name' => config('constants.STATUS.GENRE')
                [strtoupper(object_get($user, "profile.genre", 'O'))],
                'avatar'     => object_get($user, "profile.avatar", null),
                'id_number'  => object_get($user, "profile.id_number", null),
                'language'   => object_get($user, "profile.language", "VI"),
                'is_active'  => $user->is_active,
                'updated_at' => date('Y/m/d', strtotime($user->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
