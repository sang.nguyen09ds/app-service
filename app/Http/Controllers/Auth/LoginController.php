<?php
/**
 * User: Administrator
 * Date: 27/12/2018
 * Time: 10:38 PM
 */

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\CustomerProfile;
use App\CustomerSession;
use App\Http\Controllers\Controller;
use App\Http\Validators\Web\CustomerRegisterValidator;
use App\CUS;
use App\OFFICE;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\Home\Models\CustomerModel;
use App\V1\Home\Models\CustomerProfileModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Validators\Web\CustomerLoginValidator;

class LoginController extends Controller
{
    protected static $_customer_type_seller;
    protected static $_customer_type_agency;
    protected static $_customer_expired_day;
    /**
     *
     */
    protected $jwt;

    protected $model;

    /**
     * AuthController constructor.
     *
     * @param JWTAuth $jwt
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
        self::$_customer_type_seller = "SELLER";
        self::$_customer_type_agency = "AGENCY";
        self::$_customer_expired_day = 365;
        $this->model = new CustomerModel();
    }

    /**
     * @param Request $request
     * @param CustomerLoginValidator $customerLoginValidator
     *
     * @return mixed
     */
    public function authenticate(Request $request, CustomerLoginValidator $customerLoginValidator)
    {
        $input = $request->all();

        $customerLoginValidator->validate($input);

        $credentials = $request->only('code', 'password');

        try {
            $token = $this->jwt->attempt($credentials);

            if (!$token) {
                return response()->json(['errors' => [[Message::get("customers.login-invalid")]]], 401);
            }

            $customer = Customer::where(['code' => $input['code']])->first();

            if (empty($customer)) {
                return response()->json(['errors' => [[Message::get("customers.login-invalid")]]], 401);
            }

            if ($customer->is_active == "0") {
                return response()->json(['errors' => [[Message::get("customers.customer-inactive")]]], 401);
            }

            // Write Customer Session
            $now = time();
            CustomerSession::where('customer_id', $customer->id)->update([
                'deleted'    => 1,
                'updated_at' => date("Y-m-d H:i:s", $now),
                'updated_by' => $customer->id,
            ]);

            CustomerSession::where('customer_id', $customer->id)->delete();

            $device_type = array_get($input, 'device_type', 'UNKNOWN');
            CustomerSession::insert([
                'customer_id' => $customer->id,
                'token'       => $token,
                'login_at'    => date("Y-m-d H:i:s", $now),
                'expired_at'  => date("Y-m-d H:i:s", ($now + config('jwt.ttl') * 60)),
                'device_type' => $device_type,
                'device_id'   => array_get($input, 'device_id'),
                'deleted'     => 0,
                'created_at'  => date("Y-m-d H:i:s", $now),
                'created_by'  => $customer->id,
            ]);
        } catch (JWTException $e) {

            return response()->json(['errors' => [[$e->getMessage()]]], 500);
        } catch (\Exception $ex) {
            return response()->json(['errors' => [[$ex->getMessage()]]], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * @param Request $request
     * @param CustomerRegisterValidator $customerRegisterValidator
     *
     * @return array
     * @throws \Exception
     */
    public function register(Request $request, CustomerRegisterValidator $customerRegisterValidator)
    {
        $input = $request->all();

        $customerRegisterValidator->validate($input);

        $verify = uniqid() . time() . uniqid();

        try {
            DB::beginTransaction();
            $code = time();
            $customer = Customer::create([
                'phone'        => array_get($input, 'phone'),
                'code'         => $code,
                'name'         => $input['name'],
                'email'        => array_get($input, 'email'),
                'password'    => password_hash($input['password'], PASSWORD_BCRYPT),
                'verify_code' => $verify,
                'expired_code' => date('Y-m-d H:i:s', strtotime("+5 minutes")),
                'is_active'   => 1,
            ]);

            $names = explode(" ", trim($input['name']));
            $first = $names[0];
            unset($names[0]);
            $last = !empty($names) ? implode(" ", $names) : null;
            $prProfile = [
                'email'       => array_get($input, 'email'),
                'is_active'   => 1,
                'first_name'  => $first,
                'last_name'   => $last,
                'short_name'  => $input['name'],
                'full_name'   => $input['name'],
                'address'     => array_get($input, 'address', null),
                'phone'       => array_get($input, 'phone', null),
                'birthday'    => empty($input['birthday']) ? null : $input['birthday'],
                'genre'       => array_get($input, 'genre', "O"),
                //'avatar'      => $file_name ? $dir . "/" . $file_name . ".jpg" : null,
                'id_number'   => array_get($input, 'id_number', 0),
                'customer_id' => $customer->id
            ];

            $profile = CustomerProfile::where(['customer_id' => $customer->id])->first();

            // Create Profile
            $customerProfileMode = new CustomerProfileModel();
            if (empty($profile)) {
                $customerProfileMode->create($prProfile);
            } else {
                $prProfile['id'] = $profile->id;
                $customerProfileMode->update($prProfile);
            }

            DB::commit();

            $data = [
                'id'         => $customer->id,
                'first_name' => $prProfile['first_name'],
                'last_name'  => $prProfile['last_name'],
                'email'      => $customer->email,
            ];

            // Send Mail

            return ["status" => Response::HTTP_OK, "data" => $data];
        } catch (QueryException $ex) {
            DB::rollBack();
            throw new \Exception($ex);
        }
    }

    public function logout()
    {
        try {
            $customerId = CUS::getCurrentCustomerId();
            if (empty($customerId)) {
                return response()->json([
                    'message'     => Message::get('unauthorized'),
                    'status_code' => Response::HTTP_UNAUTHORIZED,
                ], Response::HTTP_UNAUTHORIZED);
            }

            CustomerSession::where('customer_id', $customerId)->where('deleted', '0')->update([
                'deleted'    => 1,
                'updated_at' => date('Y-m-d H:i:s', time()),
                'updated_by' => $customerId,
            ]);

            $token = $this->jwt->getToken();
            $this->jwt->invalidate($token);
        } catch (TokenInvalidException $exInvalid) {
            return response()->json([
                'message'     => 'A token is invalid',
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        } catch (TokenExpiredException $exExpire) {
            return response()->json([
                'message'     => 'A token is expired',
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        } catch (JWTException $jwtEx) {
            return response()->json([
                'message'     => Message::get('logout-success'),
                'status_code' => Response::HTTP_OK,
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message'     => Message::get('logout-success'),
            'status_code' => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }

    public function forgetPassword(Request $request)
    {
        $input = $request->all();
        if (empty($input['email'])) {
            return response()->json([
                'message'     => Message::get("V001", "Email"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            return response()->json([
                'message'     => Message::get("V002", "Email"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $customer = DB::table('customers')->select(['email', 'code'])->where([
            'email'     => $input['email'],
            'is_active' => 1,
        ])->first();
        if (empty($customer) || empty($customer->email) || empty($customer->code)) {
            return response()->json([
                'message'     => Message::get("V003", "Email"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $verify = base64_encode(trim(base64_encode($input['email'] . "|" . $customer->code), "=="));
            $link_to = OFFICE::urlBase("/#/reset-password?token=$verify");

            // Update Expired Date
            DB::table('customers')->select(['id'])->where(['email' => $input['email'], 'is_active' => 1])->update([
                'expired_token' => date('Y-m-d H:i:s', strtotime("+5 min")),
            ]);

//            OFFICE_Email::send(OFFICE_Email::$view_reset_password, $customer->email, [
//                'link_to' => $link_to,
//            ], null, null, "OFFICE - Reset Password!");

            return response()->json([
                'message'     => "Please check email {$input['email']} to reset new password!",
                'status_code' => Response::HTTP_OK,
            ], Response::HTTP_OK);

        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return response()->json([
                'message'     => $response['message'],
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function resetPassword(Request $request)
    {
        $input = $request->all();
        if (empty($input['token'])) {
            return response()->json([
                'message'     => Message::get("V001", "Token"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        if (empty($input['new_password'])) {
            return response()->json([
                'message'     => Message::get("V001", "New Password"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $containsUpper = preg_match('/[A-Z]/', $input['new_password']);
        $containsLower = preg_match('/[a-z]/', $input['new_password']);
        $containsNumber = preg_match('/[0-9]/', $input['new_password']);
        $containsDigit = preg_match('/\d/', $input['new_password']);
        $containsSpecial = preg_match('/[^a-zA-Z\d]/', $input['new_password']);

        if (strlen($input['new_password']) < 8 || !$containsDigit || !$containsUpper || !$containsLower || !$containsNumber || !$containsSpecial) {
            return response()->json([
                'message'     => "Password has at least one number, special char, upper case, lower case and greater than 8 digits!",
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $decodeToken = base64_decode(base64_decode($input['token']));

        if (empty($decodeToken)) {
            return response()->json([
                'message'     => Message::get("V002", "Token"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $tokens = explode("|", $decodeToken);
        if (count($tokens) == 1) {
            return response()->json([
                'message'     => Message::get("V002", "Token"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $email = $tokens[0];
        $code = $tokens[1];

        $customer = DB::table('customers')->select(['id', 'email', 'code', 'expired_token'])->where([
            'email'     => $email,
            'code'      => $code,
            'is_active' => 1,
        ])->first();

        if (empty($customer) || empty($customer->expired_token)) {
            return response()->json([
                'message'     => Message::get("V002", "Token"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        if (strtotime($customer->expired_token) < time()) {
            return response()->json([
                'message'     => Message::get("V005", "Token"),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::table('customers')->select('id')
                ->where(['email' => $email, 'code' => $code, 'is_active' => 1])
                ->update([
                    'password'      => password_hash($input['new_password'], PASSWORD_BCRYPT),
                    'expired_token' => null,
                ]);

            return response()->json([
                'message'     => "Update password successfully!",
                'status_code' => Response::HTTP_OK,
            ], Response::HTTP_OK);

        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);

            return response()->json([
                'message'     => $response['message'],
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
