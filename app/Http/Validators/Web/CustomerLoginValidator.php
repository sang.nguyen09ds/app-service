<?php
/**
 * User: Administrator
 * Date: 28/12/2018
 * Time: 09:55 PM
 */

namespace App\Http\Validators\Web;


use App\Http\Validators\ValidatorBase;
use App\Supports\Message;

class CustomerLoginValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'code'        => 'required',
            'password'    => 'required',
            'device_type' => 'in:DESKTOP,TABLET,PHONE,ANDROID,IOS,UNKNOWN',
            'device_id'   => 'nullable|max:50'
        ];
    }

    protected function attributes()
    {
        return [
            'code'        => Message::get("code"),
            'password'    => Message::get("password"),
            'device_type' => Message::get("device_type"),
            'device_id'   => Message::get("device_id"),
        ];
    }
}