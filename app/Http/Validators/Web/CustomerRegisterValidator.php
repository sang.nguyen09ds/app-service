<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 28/12/2018
 * Time: 09:56 PM
 */

namespace App\Http\Validators\Web;


use App\Http\Validators\ValidatorBase;
use App\Supports\Message;
use Illuminate\Validation\Rule;

class CustomerRegisterValidator extends ValidatorBase
{
    protected $email;

    protected function rules()
    {
        return [
            'device_id'   => 'required',
            'device_type' => 'required',
            'email'       => [
                'required',
                'email',
                Rule::unique("customers")->where(function ($query) {
                    return $query->where('email', $this->email)
                        ->whereNull('deleted_at');
                })
            ],
            'name'        => 'required|min:5|max:40',
            'password'    => 'required|min:8',
        ];
    }

    protected function attributes()
    {
        return [
            'email'       => Message::get("email"),
            'device_id'   => Message::get("device_id"),
            'device_type' => Message::get("device_type"),
            'name'        => Message::get("name"),
            'city_id'     => Message::get("city_id"),
            'district_id' => Message::get("district_id"),
            'ward_id'     => Message::get("ward_id"),
            'password'    => Message::get("password"),
        ];
    }

    /**
     * @param $input
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate($input)
    {
        if (!empty($input['email'])) {
            $this->email = $input['email'];
        }
        parent::validate($input);
    }
}