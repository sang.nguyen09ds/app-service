<?php
/**
 * User: Administrator
 * Date: 16/10/2018
 * Time: 08:07 PM
 */

namespace App;


class Contact extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    protected $fillable = [
        "subject_id",
        "subject_code",
        "subject_name",
        "subject_desc",
        "about_id",
        "about_code",
        "about_name",
        "about_desc",
        "image",
        "user_id",
        "content",
        "status",
        "is_active",
        "deleted",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];

    public function user()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'id', 'user_id');
    }
}
