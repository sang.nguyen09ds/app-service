<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 6:31 PM
 */

namespace App;


class SizeColor extends BaseModel
{
    protected $table = 'size_color';

    protected $fillable = [
        'size_id',
        'color_id',
        'product_id',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];
}
