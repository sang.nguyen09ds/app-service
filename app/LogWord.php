<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 5/21/2019
 * Time: 11:58 PM
 */

namespace App;


class LogWord extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'log_word';

    /**
     * @var array
     */
    protected $fillable = [
        'issue_id',
        'time_spent',
        'date_started',
        'work_description',
        'is_active',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    public function issue()
    {
        return $this->hasOne(__NAMESPACE__ . '\Issue', 'id', 'issue_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'id', 'created_by');
    }
}