<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:45 PM
 */

namespace App;


class CustomerProfile extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_profiles';


    /**
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'email',
        'first_name',
        'last_name',
        'short_name',
        'full_name',
        'address',
        'phone',
        'birthday',
        'avatar',
        'genre',
        'tax_number',
        'account_number',
        'bank_type',
        'spokesman',
        'branch_name',
        'receipt_address',
        'id_number',
        'is_active',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'id', 'customer_id');
    }
}