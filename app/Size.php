<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 6:26 PM
 */

namespace App;


class Size extends BaseModel
{
    protected $table = 'size_app';

    protected $fillable = [
        'name',
        'title',
        'is_active',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    public function images()
    {
        return $this->hasMany(__NAMESPACE__ . '\ImageProduct', 'product_id', 'id');
    }
}
