<?php

namespace App\V1\Home\Controllers;

use App\V1\Home\Models\ProductModel;
use App\V1\Home\Transformers\Product\ProductDetailTransformer;
use App\V1\Home\Transformers\Product\ProductListTransformer;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\V1\CMS\Controllers
 */
class ProductController extends BaseController
{

    /**
     * @var ProductModel
     */
    protected $model;

    /**
     * ProductController constructor.
     * @param ProductModel $model
     */
    public function __construct(ProductModel $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function index()
    {
        return ['product-status' => '0k'];
    }

    /**
     * @param Request $request
     * @param ProductListTransformer $productListTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getList(Request $request, ProductListTransformer $productListTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }
        $productModel = new ProductModel();
        $products = $productModel->search($input, [], $limit);

        return $this->response->paginator($products, $productListTransformer);
    }

    /**
     * @param $id
     * @param Request $request
     * @param ProductDetailTransformer $productDetailTransformer
     *
     * @return \Dingo\Api\Http\Response|string
     */
    public function getDetail($id, Request $request, ProductDetailTransformer $productDetailTransformer)
    {
        $input = $request->all();
        $input['id'] = $id;
        $productModel = new ProductModel();

        $product = $productModel->search($input, [], 1);

        return $this->response->item($product, $productDetailTransformer);
    }
}
