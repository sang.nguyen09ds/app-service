<?php
/**
 * User: Administrator
 * Date: 30/12/2018
 * Time: 12:09 AM
 */

namespace App\V1\Home\Controllers;


use App\V1\CMS\Transformers\Category\CategoryTransformer;
use App\V1\Home\Models\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{

    protected $model;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new CategoryModel();
    }

    /**
     * @param Request $request
     * @param CategoryTransformer $categoryTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function getList(Request $request, CategoryTransformer $categoryTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }
        $categoryModel = new CategoryModel();
        $categories = $categoryModel->search($input, [], $limit);
        return $this->response->paginator($categories, $categoryTransformer);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function getDetail($id, Request $request)
    {
        $input = $request->all();

        $category = $this->model->show($id);

        return ["status" => Response::HTTP_OK, "data" => $category];
    }
}