<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:48 PM
 */

namespace App\V1\Home\Controllers;


use App\CUS;
use App\Customer;
use App\CustomerProfile;
use App\OFFICE;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\CMS\Validators\UserUpdateValidator;
use App\V1\Home\Models\CustomerModel;
use App\V1\Home\Transformers\Customer\CustomerProfileTransformer;
use App\V1\Home\Validators\CustomerUpdateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends BaseController
{

    /**
     * @var CustomerModel
     */
    protected $model;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->model = new CustomerModel();
    }

    /**
     * @param Request $request
     * @param CustomerProfileTransformer $customerTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, CustomerProfileTransformer $customerTransformer)
    {
        $input = $request->all();
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }

        $limit = array_get($input, 'limit', 20);
        $customer = $this->model->search($input, ['profile'], $limit);
        return $this->response->paginator($customer, $customerTransformer);
    }

    /**
     * @param $code
     * @param CustomerProfileTransformer $customerTransformer
     * @return array|\Dingo\Api\Http\Response
     */
    public function detail($code, CustomerProfileTransformer $customerTransformer)
    {
        $customer = Customer::model()->with(['profile'])->where('code', $code)->first();
        if (empty($customer)) {
            return ["data" => []];
        }
        return $this->response->item($customer, $customerTransformer);
    }

    public function info(CustomerProfileTransformer $customerProfileTransformer)
    {
        $customer = Customer::model()->where('id', CUS::getCurrentCustomerId())->first();
        if (empty($customer)) {
            return $this->response->errorBadRequest(Message::get('V003', Message::get("customers")));
        }
        return $this->response->item($customer, $customerProfileTransformer);
    }

    public function infoByCode($code, CustomerProfileTransformer $customerProfileTransformer)
    {
        $customer = Customer::model()->where('code', $code)->first();
        if (empty($customer)) {
            return $this->response->errorBadRequest(Message::get('V003', "ID #$code"));
        }
        return $this->response->item($customer, $customerProfileTransformer);
    }

    public function updateProfile(Request $request, CustomerUpdateValidator $customerUpdateValidator, UserUpdateValidator $userUpdateValidators, CustomerProfileTransformer $customerProfileTransformer) {

        if(CUS::getCurrentCustomerType() == USER_TYPE_USER){

             $this->updateUser($request, $userUpdateValidators);
        } else {
            $customer= $this->updateCustomer($request, $customerUpdateValidator);
            return $this->response->item($customer, $customerProfileTransformer);
        }
    }

    private function updateCustomer(Request $request, CustomerUpdateValidator $customerUpdateValidator)
    {
        $input = $request->all();
        if (empty($input)) {
            return ['status' => Message::get("customers.update-success", Message::get("profile"))];
        }
        $customerUpdateValidator->validate($input);
        $id = CUS::getCurrentCustomerId();
        if (empty($id)) {
            return $this->response->errorBadRequest(Message::get('V003', Message::get("customers")));
        }

        $customer = Customer::find($id);
        if (empty($customer)) {
            return $this->response->errorBadRequest(Message::get('V003', "ID #$id"));
        }

        try {
            DB::beginTransaction();
            $customerProfile = CustomerProfile::model()->where('customer_id', $id)->first();
            if (empty($customerProfile)) {
                $customerProfile = new CustomerProfile();
                $customerProfile->customer_id = $id;
            }

            if (!empty($input['name'])) {
                $customer->name = $input['name'];
                $customerProfile->full_name = $input['name'];
                $full = explode(" ", $input['name']);

                $customerProfile->first_name = trim($full[count($full) - 1]);
                unset($full[count($full) - 1]);
                $customerProfile->last_name = implode(" ", $full);
            }

            if (!empty($input['email'])) {
                $customer->email = $input['email'];
            }

            if (!empty($input['avatar'])) {
                $avatar = explode(';base64,', $input['avatar']);
                if (!empty($avatar[1])) {
                    $data = base64_decode($avatar[1]);
                    file_put_contents(public_path() . "/uploads/avatar/user-" . $id . ".png",
                        $data);
                    $customerProfile->avatar = "uploads,avatar,user-" . $id . ".png";
                }
            }

            $customerProfile->save();
            $customer->save();
            return  $customer;
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("customers.update-success", $customer->name)];
    }

    private function updateUser(Request $request, CustomerUpdateValidator $customerUpdateValidator)
    {
        $input = $request->all();
        if (empty($input)) {
            return ['status' => Message::get("customers.update-success", Message::get("profile"))];
        }
        $customerUpdateValidator->validate($input);
        $id = CUS::getCurrentCustomerId();
        if (empty($id)) {
            return $this->response->errorBadRequest(Message::get('V003', Message::get("customers")));
        }

        $customer = Customer::find($id);
        if (empty($customer)) {
            return $this->response->errorBadRequest(Message::get('V003', "ID #$id"));
        }

        try {
            DB::beginTransaction();
            $customerProfile = CustomerProfile::model()->where('customer_id', $id)->first();
            if (empty($customerProfile)) {
                $customerProfile = new CustomerProfile();
                $customerProfile->customer_id = $id;
            }

            if (!empty($input['name'])) {
                $customer->name = $input['name'];
                $customerProfile->full_name = $input['name'];
                $full = explode(" ", $input['name']);

                $customerProfile->first_name = trim($full[count($full) - 1]);
                unset($full[count($full) - 1]);
                $customerProfile->last_name = implode(" ", $full);
            }

            if (!empty($input['email'])) {
                $customer->email = $input['email'];
            }

            if (!empty($input['avatar'])) {
                $avatar = explode(';base64,', $input['avatar']);
                if (!empty($avatar[1])) {
                    $data = base64_decode($avatar[1]);
                    file_put_contents(public_path() . "/uploads/avatar/user-" . $id . ".png",
                        $data);
                    $customerProfile->avatar = "uploads,avatar,user-" . $id . ".png";
                }
            }

            $customerProfile->save();
            $customer->save();

            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("customers.update-success", $customer->name)];
    }
}
