<?php
/**
 * User: kpistech2
 * Date: 2019-01-26
 * Time: 22:53
 */

namespace App\V1\Home\Controllers;


use App\CUS;
use App\Customer;
use App\User;
use App\V1\Home\Models\UserModel;
use App\V1\Home\Transformers\User\UserProfileTransformer;
use App\V1\Home\Transformers\User\UserTransformer;
use Illuminate\Http\Request;

class UserController extends BaseController
{

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * @param Request $request
     * @param UserTransformer $userTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, UserTransformer $userTransformer)
    {
        $input = $request->all();
        $input['type'] = USER_TYPE_SELLER;
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }
        $limit = array_get($input, 'limit', 20);
        $user = $this->model->search($input, ['profile'], $limit);
        return $this->response->paginator($user, $userTransformer);
    }

    /**
     * @param $id
     * @param UserTransformer $userTransformer
     * @return array|\Dingo\Api\Http\Response
     */
    public function view($id, UserTransformer $userTransformer)
    {
        $user = User::with(['profile'])->where('id', $id)->where('type', USER_TYPE_SELLER)->first();
        if (empty($user)) {
            return ["data" => []];
        }
        return $this->response->item($user, $userTransformer);
    }

    /**
     * @param UserProfileTransformer $userProfileTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function info(UserProfileTransformer $userProfileTransformer)
    {
        $id = CUS::getCurrentCustomerId();
        if (CUS::getCurrentCustomerType() === USER_TYPE_CUSTOMER) {
            $user = Customer::find($id);
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return $this->response->errorBadRequest(Message::get('V003', "ID #$id"));
        }
        return $this->response->item($user, $userProfileTransformer);
    }
}
