<?php
/**
 * User: kpistech2
 * Date: 2019-01-28
 * Time: 00:00
 */

namespace App\V1\Home\Controllers;


use App\Price;
use App\V1\Home\Models\PriceModel;
use App\V1\Home\Transformers\Price\PriceDetailTransformer;
use App\V1\Home\Transformers\Price\PriceTransformer;
use Illuminate\Http\Request;

class PriceController extends BaseController
{

    protected $model;

    /**
     * PriceController constructor.
     *
     * @param PriceModel $model
     */
    public function __construct(PriceModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     * @param PriceTransformer $priceTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, PriceTransformer $priceTransformer)
    {
        $input = $request->all();
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }
        $limit = array_get($input, 'limit', 20);
        $price = $this->model->search($input, [], $limit);

        return $this->response->paginator($price, $priceTransformer);
    }

    /**
     * @param $id
     * @param PriceTransformer $priceTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function detail($id, PriceTransformer $priceTransformer)
    {
        $price = Price::find($id);
        if (empty($price)) {
            return ['data' => ''];
        }

        return $this->response->item($price, $priceTransformer);
    }

    ////////////////////////////// Price Detail /////////////////////////

    /**
     * @param $code
     * @param PriceDetailTransformer $priceDetailTransformer
     *
     * @return array|\Dingo\Api\Http\Response
     */
    public function priceDetail($code, PriceDetailTransformer $priceDetailTransformer)
    {
        try {
            $price = Price::model()->with(['details'])->where('code', $code)->first();

            if (empty($price)) {
                return ['data' => ''];
            }

        } catch (\Exception $exception) {
            return ['data' => $exception->getMessage()];
        }
        return $this->response->item($price, $priceDetailTransformer);
    }
}
