<?php
/**
 * User: kpistech2
 * Date: 2019-01-12
 * Time: 22:22
 */

namespace App\V1\Home\Controllers;


use App\CUS;
use App\Order;
use App\Supports\OFFICE_Error;
use App\V1\Home\Models\OrderModel;
use App\V1\Home\Transformers\Order\OrderTransformer;
use App\V1\Home\Validators\OrderCreateValidator;
use App\V1\Home\Validators\OrderUpdateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends BaseController
{

    /**
     * @var OrderModel
     */
    protected $model;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        /** @var OrderModel model */
        $this->model = new OrderModel();
    }

    /**
     * @param Request $request
     * @param OrderTransformer $orderTransformer
     *
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, OrderTransformer $orderTransformer)
    {
        $input = $request->all();
        if (empty($input['customer_id'])) {
            $input['customer_id'] = CUS::getCurrentCustomerId();
        }
        $limit = array_get($input, 'limit', 20);
        $order = $this->model->search($input, ['price', 'seller', 'customer', 'customer.profile', 'details'], $limit);
        return $this->response->paginator($order, $orderTransformer);
    }

    /**
     * @param $code
     * @param OrderTransformer $orderTransformer
     * @return array|\Dingo\Api\Http\Response
     */
    public function view($code, OrderTransformer $orderTransformer)
    {
        $order = Order::model()->with(['price', 'seller', 'customer', 'customer.profile', 'details'])->where('code',
            $code)->where('customer_id', CUS::getCurrentCustomerId())->first();

        if (empty($order)) {
            return ['data' => []];
        }
        return $this->response->item($order, $orderTransformer);
    }

    public function create(
        Request $request,
        OrderCreateValidator $orderCreateValidator,
        OrderTransformer $orderTransformer
    ) {
        $input = $request->all();
        $orderCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $order = $this->model->upsert($input);
            $this->sendMessageToToken($order->id, $order->code);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($order, $orderTransformer);
    }
    public static function sendMessageToToken($id, $code)
    {
        $now = time();
        $data = [
            'title'     => 'Thông báo đặt hàng thành công',
            'body'      => 'Đơn hàng ' . $code,
            'message'   => "Đây là message",
            'extraData' => 'extraData', // anyType
            'date'      => date("Y-m-d H:i:s", $now),
            'type'      => 1,
            'action'    => 1,
            'id'  => $id,
        ];
        $notification = array(
            'title' => 'Thông báo đặt hàng thành công',
            'body'  => 'Đơn hàng ' . $code,
        );
        $deviceToken = CUS::getCurrentCustomerDevice_id();
        {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $serverKey = "AAAAGGjp0qQ:APA91bG0xp6eFNFjWyelshEYPVQAmPIc3-vu77bBynkWgesNPCEXSPLQFazfRQmZqDozeyQh-T_dtvyWPps4_yKi3YWM-bDtLBw9hggfC9OlIMNOmIi6Ci4aa_7S5HfA6cnfytMWHsmw";
            // $serverKey = "AIzaSyDDhcMWAg2-frKtQH9_7yrv39pgO35YGeQ";
            $fields = array();
            $fields['data'] = $data;
            $fields['notification'] = $notification;

            if (is_array($deviceToken)) {
                $fields['registration_ids'] = $deviceToken; // Gửi nhiều thiết bị
            } else {
                $fields['to'] = $deviceToken; // Gửi một thiết bị
            }
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $serverKey
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);
            if ($result === false) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);

        }
    }

    public function update(
        $id,
        Request $request,
        OrderUpdateValidator $orderUpdateValidator,
        OrderTransformer $orderTransformer
    ) {
        $input = $request->all();
        $input['id'] = $id;
        $orderUpdateValidator->validate($input);

        try {
            DB::beginTransaction();
            $order = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($order, $orderTransformer);
    }
}
