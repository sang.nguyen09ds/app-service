<?php
/**
 * User: kpistech2
 * Date: 2019-01-27
 * Time: 23:28
 */

namespace App\V1\Home\Controllers;


use App\V1\Home\Models\WarehouseModel;
use App\V1\Home\Transformers\Warehouse\WarehouseTransformer;
use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends BaseController
{

    protected $model;

    /**
     * WarehouseController constructor.
     * @param WarehouseModel $model
     */
    public function __construct(WarehouseModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Request $request
     * @param WarehouseTransformer $warehouseTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, WarehouseTransformer $warehouseTransformer)
    {
        $input = $request->all();
        if (!empty($input['keyword'])) {
            $input['name'] = ['like' => $input['keyword']];
        }
        $limit = array_get($input, 'limit', 20);
        $warehouse = $this->model->search($input, [], $limit);

        return $this->response->paginator($warehouse, $warehouseTransformer);
    }

    /**
     * @param $id
     * @param WarehouseTransformer $warehouseTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function detail($id, WarehouseTransformer $warehouseTransformer)
    {
        $warehouse = Warehouse::find($id);
        if (empty($warehouse)) {
            return ['data' => ''];
        }

        return $this->response->item($warehouse, $warehouseTransformer);
    }
}
