<?php
/**
 * User: kpistech2
 * Date: 2019-01-13
 * Time: 00:10
 */

namespace App\V1\Home\Controllers;


use App\Cart;
use App\CUS;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\Home\Models\CartModel;
use App\V1\Home\Transformers\Cart\CartTransformer;
use App\V1\Home\Validators\CartUpdateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends BaseController
{

    /**
     * @var CartModel
     */
    protected $model;

    /**
     * CartController constructor.
     */
    public function __construct()
    {
        /** @var CartModel model */
        $this->model = new CartModel();
    }

    /**
     * @param $code
     * @param CartTransformer $cartTransformer
     * @return array|\Dingo\Api\Http\Response
     */
    public function view(CartTransformer $cartTransformer)
    {
        $cart = Cart::model()->with(['details', 'customer'])->where('customer_id', CUS::getCurrentCustomerId())
            ->first();
        if (empty($cart)) {
            return ['data' => []];
        }
        return $this->response->item($cart, $cartTransformer);
    }

    /**
     * @param $id
     * @param Request $request
     * @param CartUpdateValidator $cartUpdateValidator
     * @param CartTransformer $cartTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(
        Request $request,
        CartUpdateValidator $cartUpdateValidator,
        CartTransformer $cartTransformer
    ) {
        $input = $request->all();

        $cartUpdateValidator->validate($input);

        $cart = Cart::model()->where('customer_id', CUS::getCurrentCustomerId())->first();
        if (empty($cart)) {
            // Create Card
            $card = new Cart();
            $card->code = strtoupper(uniqid());
            $card->name = $card->code;
            $card->customer = CUS::getCurrentCustomerId();
            $card->save();
        }

        $input['id'] = $card->id;

        try {
            DB::beginTransaction();
            $cart = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($cart, $cartTransformer);
    }
}
