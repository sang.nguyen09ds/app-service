<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 08:08 PM
 */

namespace App\V1\Home\Validators;

use App\Http\Validators\ValidatorBase;
use App\Supports\Message;

class CustomerUpdateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'password' => 'nullable|min:8',
            'phone'    => 'max:12',
            'name'     => 'min:5',
        ];
    }

    protected function attributes()
    {
        return [
            'phone'    => Message::get("phone"),
            'name'     => Message::get("name"),
            'password' => Message::get("password"),
        ];
    }
}