<?php
/**
 * User: kpistech2
 * Date: 2019-01-27
 * Time: 00:04
 */

namespace App\V1\Home\Validators;


use App\Http\Validators\ValidatorBase;
use App\Order;
use App\Supports\Message;
use Illuminate\Http\Request;

class OrderUpdateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id'                     => 'required|exists:orders,id,deleted_at,NULL',
            'code'                   => [
                'nullable',
                'max:50',
                function ($attribute, $value, $fail) {
                    $input = Request::capture();
                    $item = Order::model()->where('code', $value)->get()->toArray();
                    if (!empty($item) && count($item) > 0) {
                        if (count($item) > 1 || ($input['id'] > 0 && $item[0]['id'] != $input['id'])) {
                            return $fail(Message::get("unique", "$attribute: #$value"));
                        }
                    }
                }
            ],
            'customer_id'            => 'required|exists:customers,id,deleted_at,NULL',
            'customer_name'          => 'max:50',
            'customer_address'       => 'max:300',
            'customer_phone'         => 'max:15',
            'receiver_id'            => 'nullable|exists:customers,id,deleted_at,NULL',
            'receiver_name'          => 'max:50',
            'receiver_address'       => 'max:300',
            'receiver_phone'         => 'max:15',
            'order_date'             => 'date_format:Y-m-d',
            'pay_later'              => 'integer|max:1',
            'price_id'               => 'nullable|integer|exists:prices,id,deleted_at,NULL',
            'ship_date'              => 'date_format:Y-m-d',
            'ship_time'              => 'max:50',
            'seller_id'              => 'exists:users,id,deleted_at,NULL',
            'seller_name'            => 'max:50',
            'seller_phone'           => 'max:15',
            'details'                => 'required|array',
            'details.*.product_id'   => 'required|exists:products,id,deleted_at,NULL',
            'details.*.warehouse_id' => 'required|exists:warehouses,id,deleted_at,NULL',
            'details.*.qty'          => 'required',
            'details.*.price'        => 'nullable|numeric',
        ];
    }

    protected function attributes()
    {
        return [
            'customer_id'            => Message::get("customer_id"),
            'customer_name'          => Message::get("customer_name"),
            'customer_address'       => Message::get("customer_address"),
            'customer_phone'         => Message::get("customer_phone"),
            'receiver_id'            => Message::get("receiver_id"),
            'receiver_name'          => Message::get("receiver_name"),
            'receiver_address'       => Message::get("receiver_address"),
            'receiver_phone'         => Message::get("receiver_phone"),
            'order_date'             => Message::get("order_date"),
            'pay_later'              => Message::get("pay_later"),
            'price_id'               => Message::get("prices"),
            'ship_date'              => Message::get("ship_date"),
            'ship_time'              => Message::get("ship_time"),
            'seller_id'              => Message::get("users"),
            'seller_name'            => Message::get("alternative_name"),
            'seller_phone'           => Message::get("phone"),
            'details'                => Message::get("detail"),
            'details.*.qty'          => Message::get("quantity"),
            'details.*.price'        => Message::get("prices"),
            'details.*.product_id'   => Message::get("product_id"),
            'details.*.warehouse_id' => Message::get("warehouses"),
        ];
    }
}
