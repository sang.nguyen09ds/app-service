<?php
/**
 * User: kpistech2
 * Date: 2019-01-27
 * Time: 00:00
 */

namespace App\V1\Home\Validators;


use App\Http\Validators\ValidatorBase;
use App\Order;
use App\Supports\Message;
use Illuminate\Http\Request;

class OrderCreateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'code'                   => [
                'nullable',
                'max:50',
                function ($attribute, $value, $fail) {
                    $order = Order::model()->where('code', $value)->first();
                    if (!empty($order)) {
                        return $fail(Message::get("unique", "$attribute: #$value"));
                    }
                }
            ],
            'customer_id'            => 'required|exists:customers,id,deleted_at,NULL',
            'customer_name'          => 'max:50',
            'customer_address'       => 'max:300',
            'customer_phone'         => 'max:15',
            'receiver_id'            => 'nullable|exists:customers,id,deleted_at,NULL',
            'receiver_name'          => 'max:50',
            'receiver_address'       => 'max:300',
            'receiver_phone'         => 'max:15',
            'order_date'             => 'date_format:Y-m-d',
            'price_id'               => 'required|integer|exists:prices,id,deleted_at,NULL',
            'pay_later'              => 'integer|max:1',
            'ship_date'              => 'date_format:Y-m-d',
            'ship_time'              => 'max:50',
            'seller_id'              => 'exists:users,id,deleted_at,NULL',
            'seller_name'            => 'max:50',
            'seller_phone'           => 'max:15',
            'details'                => 'required|array',
            'details.*.product_id'   => 'required|exists:products,id,deleted_at,NULL',
            'details.*.warehouse_id' => 'required|exists:warehouses,id,deleted_at,NULL',
            'details.*.qty'          => 'required',
            'details.*.price'        => 'nullable|numeric',
        ];
    }

    protected function attributes()
    {
        return [
            'code'                   => Message::get("code"),
            'customer_id'            => Message::get("customer_id"),
            'customer_name'          => Message::get("customer_name"),
            'customer_address'       => Message::get("customer_address"),
            'customer_phone'         => Message::get("customer_phone"),
            'receiver_id'            => Message::get("receiver_id"),
            'receiver_name'          => Message::get("receiver_name"),
            'receiver_address'       => Message::get("receiver_address"),
            'receiver_phone'         => Message::get("receiver_phone"),
            'order_date'             => Message::get("order_date"),
            'price_id'               => Message::get("prices"),
            'pay_later'              => Message::get("pay_later"),
            'ship_date'              => Message::get("ship_date"),
            'ship_time'              => Message::get("ship_time"),
            'seller_id'              => Message::get("users"),
            'seller_name'            => Message::get("alternative_name"),
            'seller_phone'           => Message::get("phone"),
            'details'                => Message::get("detail"),
            'details.*.qty'          => Message::get("quantity"),
            'details.*.price'        => Message::get("price"),
            'details.*.product_id'   => Message::get("product_id"),
            'details.*.warehouse_id' => Message::get("warehouses"),
        ];
    }
}
