<?php
/**
 * User: kpistech2
 * Date: 2019-01-13
 * Time: 00:37
 */

namespace App\V1\Home\Validators;


use App\Http\Validators\ValidatorBase;
use App\Supports\Message;

class CartUpdateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id'                   => 'required|exists:orders,id,deleted_at,NULL',
            'details'              => 'required|array',
            'details.*.product_id' => 'required|exists:products,id,deleted_at,NULL',
            'details.*.qty'        => 'required',
        ];
    }

    protected function attributes()
    {
        return [
            'customer_id'          => Message::get("customer_id"),
            'details'              => Message::get("detail"),
            'details.*.qty'        => Message::get("quantity"),
            'details.*.product_id' => Message::get("product_id"),
        ];
    }
}
