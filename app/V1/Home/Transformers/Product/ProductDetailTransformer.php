<?php
/**
 * User: Dai Ho
 * Date: 22-Mar-17
 * Time: 23:43
 */

namespace App\V1\Home\Transformers\Product;

use App\Supports\OFFICE_Error;
use App\Product;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductDetailTransformer
 * @package App\V1\Home\Transformers\Product
 */
class ProductDetailTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        try {
            $output = [
                'id'         => $product->id,
                'name'       => $product->name,
                'code'       => $product->code,
                'unit'       => $product->unit,
                'color'      => $product->color,
                'thumbnail'  => $product->thumbnail,
                'image'      => $product->image,
                'created_at' => date('d/m/Y H:i', strtotime($product->created_at)),
                'qty_remain' => $product->qty_remain,
            ];

            return $output;
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
