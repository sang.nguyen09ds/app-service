<?php
/**
 * User: Administrator
 * Date: 29/12/2018
 * Time: 04:10 PM
 */

namespace App\V1\Home\Transformers\Customer;


use App\Customer;
use League\Fractal\TransformerAbstract;

class CustomerProfileByQRTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        try {
            $avatar = !empty($customer->profile->avatar) ? url('/v0') . "/img/" . $customer->profile->avatar : null;
            $address = object_get($customer, "profile.address", null);
            return [
                'id'        => $customer->id,
                'code'      => $customer->code,
                'name'      => $customer->name,
                'phone'     => $customer->phone,
                'email'     => $customer->email,
                'type'      => $customer->type,
                'type_name' => object_get($customer, 'type.name'),

                'point'      => $customer->point,
                'used_point' => $customer->used_point,

                'first_name'      => object_get($customer, "profile.first_name", null),
                'last_name'       => object_get($customer, "profile.last_name", null),
                'short_name'      => object_get($customer, "profile.short_name", null),
                'full_name'       => object_get($customer, "profile.full_name", null),
                'address'         => $address,
                'receipt_address' => object_get($customer, "profile.receipt_address", $address),
                'birthday'        => object_get($customer, 'profile.birthday', null),
                'genre'           => object_get($customer, "profile.genre", "O"),
                'genre_name'      => config('constants.STATUS.GENRE')
                [strtoupper(object_get($customer, "profile.genre", 'O'))],
                'avatar'          => $avatar,
                'id_number'       => object_get($customer, "profile.id_number", null),
                'is_active'       => $customer->is_active,
                'created_at'      => date('d/m/Y H:i', strtotime($customer->created_at)),
                'updated_at'      => date('d/m/Y H:i', strtotime($customer->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = SSC_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
