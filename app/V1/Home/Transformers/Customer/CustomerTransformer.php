<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:56 PM
 */

namespace App\V1\Home\Transformers\Customer;


use App\Customer;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        try {
            $avatar = !empty($customer->profile->avatar) ? url('/v0') . "/img/" . $customer->profile->avatar : null;
            return [
                'id'    => $customer->id,
                'name'  => $customer->name,
                'full_name'  => object_get($customer, "profile.full_name", null),
                'avatar'     => $avatar,
                'created_at' => date('d/m/Y H:i', strtotime($customer->created_at)),
                'updated_at' => date('d/m/Y H:i', strtotime($customer->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
