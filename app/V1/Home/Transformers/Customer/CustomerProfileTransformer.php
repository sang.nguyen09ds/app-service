<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:58 PM
 */

namespace App\V1\Home\Transformers\Customer;


use App\Customer;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class CustomerProfileTransformer extends TransformerAbstract
{
    public function transform(Customer $customer)
    {
        $avatar = !empty($customer->profile->avatar) ? url('/v0') . "/img/" . $customer->profile->avatar : null;
        $address = object_get($customer, "profile.address", null);
        try {
            return [
                'id'        => $customer->id,
                'code'      => $customer->code,
                'name'      => $customer->name,
                'phone'     => $customer->phone,
                'email'     => $customer->email,
                'type'      => $customer->type,
                'type_name' => object_get($customer, 'type.name'),

                'point'      => $customer->point,
                'used_point' => $customer->used_point,

                'first_name'      => object_get($customer, "profile.first_name", null),
                'last_name'       => object_get($customer, "profile.last_name", null),
                'short_name'      => object_get($customer, "profile.short_name", null),
                'full_name'       => object_get($customer, "profile.full_name", null),
                'address'         => $address,
                'receipt_address' => object_get($customer, "profile.address", $address),
                'avatar'          => $avatar,
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
