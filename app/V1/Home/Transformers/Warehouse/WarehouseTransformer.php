<?php
/**
 * User: kpistech2
 * Date: 2019-01-27
 * Time: 23:33
 */

namespace App\V1\Home\Transformers\Warehouse;


use App\Supports\OFFICE_Error;
use App\Warehouse;
use League\Fractal\TransformerAbstract;

class WarehouseTransformer extends TransformerAbstract
{
    public function transform(Warehouse $warehouse)
    {
        try {
            return [
                'id'          => $warehouse->id,
                'code'        => $warehouse->code,
                'name'        => $warehouse->name,
                'address'     => $warehouse->address,
                'description' => $warehouse->description,
                'is_active'   => $warehouse->is_active,
                'updated_at'  => !empty($warehouse->updated_at) ? date('d/m/Y H:i',
                    strtotime($warehouse->updated_at)) : null,
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
