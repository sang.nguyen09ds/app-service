<?php
/**
 * User: kpistech2
 * Date: 2019-01-26
 * Time: 22:58
 */

namespace App\V1\Home\Transformers\User;


use App\Supports\OFFICE_Error;
use App\User;
use League\Fractal\TransformerAbstract;

class UserProfileTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        try {
            return [
                'id' => $user->id,
                'code' => $user->code,
                'email' => $user->email,
                'first_name' => object_get($user, "profile.first_name", null),
                'last_name' => object_get($user, "profile.last_name", null),
                'short_name' => object_get($user, "profile.short_name", null),
                'full_name' => object_get($user, "profile.full_name", null),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
