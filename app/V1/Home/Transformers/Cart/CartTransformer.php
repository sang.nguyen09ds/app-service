<?php
/**
 * User: kpistech2
 * Date: 2019-01-13
 * Time: 00:20
 */

namespace App\V1\Home\Transformers\Cart;


use App\Cart;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class CartTransformer extends TransformerAbstract
{
    public function transform(Cart $cart)
    {
        $details = $cart->details;
        $cartDetails = [];
        foreach ($details as $detail) {
            $cartDetails[] = [
                'product_name'        => object_get($detail, 'product.name'),
                'product_alias'       => object_get($detail, 'product.alias'),
                'product_description' => object_get($detail, 'product.description'),
                'product_unit'        => object_get($detail, 'product.unit'),
                'product_thumbnail'   => object_get($detail, 'product.thumbnail'),
                'product_image'       => object_get($detail, 'product.image'),
                'product_color'       => object_get($detail, 'product.color'),
                'qty'                 => $detail->qty,
            ];
        }
        try {
            return [
                'code'        => $cart->code,
                'customer_id' => $cart->customer_id,
                'description' => $cart->description,
                'details'     => $cartDetails,
                'is_active'   => $cart->is_active,
                'created_at'  => date('d/m/Y H:i', strtotime($cart->created_at)),
                'updated_at'  => date('d/m/Y H:i', strtotime($cart->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
