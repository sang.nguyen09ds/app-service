<?php
/**
 * User: kpistech2
 * Date: 2019-01-12
 * Time: 22:30
 */

namespace App\V1\Home\Transformers\Order;


use App\Order;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    public function transform(Order $order)
    {
        $details = $order->details;
        $orderDetails = [];
        foreach ($details as $detail) {
            $orderDetails[] = [
                'order_id'            => $detail->order_id,
                'product_id'          => $detail->product_id,
                'product_code'        => object_get($detail, 'product.code'),
                'product_name'        => object_get($detail, 'product.name'),
                'product_alias'       => object_get($detail, 'product.alias'),
                'product_description' => object_get($detail, 'product.description'),
                'product_unit'        => object_get($detail, 'product.unit'),
                'product_thumbnail'   => object_get($detail, 'product.thumbnail'),
                'product_image'       => object_get($detail, 'product.image'),
                'product_color'       => object_get($detail, 'product.color'),

                'warehouse_id'   => $detail['warehouse_id'],
                'warehouse_code' => object_get($detail, 'warehouse.code'),
                'warehouse_name' => object_get($detail, 'warehouse.name'),

                'qty'         => $detail->qty,
                'description' => object_get($detail, 'description'),
                'decrement'   => $detail['decrement'] ? true : null,
            ];
        }
        try {
            return [
                'id'         => $order->id,
                'code'       => $order->code,
                'order_date' => date("Y-m-d", strtotime($order->order_date)),

                'price_id'   => $order->price_id,
                'price_code' => object_get($order, "price.code"),
                'price_name' => object_get($order, "price.name"),
                'price_show' => object_get($order, "price.is_show"),

                'status'    => $order->status,
                'pay_later' => $order->pay_later,

                'customer_id'              => $order->customer_id,
                'customer_name'            => $order->customer_name,
                'customer_address'         => $order->customer_address,
                'customer_receipt_address' => object_get($order, 'customer.profile.receipt_address',
                    $order->customer_address),
                'customer_phone'           => $order->customer_phone,
                'customer_tax'             => object_get($order, 'customer.profile.tax_number'),

                'receiver_id'      => $order->receiver_id,
                'receiver_name'    => $order->receiver_name,
                'receiver_address' => $order->receiver_address,
                'receiver_phone'   => $order->receiver_phone,
                'ship_date'        => $order->ship_date,
                'ship_time'        => $order->ship_time,

                'seller_id'    => $order->seller_id,
//                'seller_name'  => $order->seller_name,
//                'seller_phone' => $order->seller_phone,
                'seller_name'  => object_get($order, 'seller.profile.full_name'),
                'seller_phone' => object_get($order, 'seller.phone'),

                'description' => $order->description,
                'details'     => $orderDetails,

                'is_active'  => $order->is_active,
                'created_at' => date('d/m/Y H:i', strtotime($order->created_at)),
                'updated_at' => date('d/m/Y H:i', strtotime($order->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}

