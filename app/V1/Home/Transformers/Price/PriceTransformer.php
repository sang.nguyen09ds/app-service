<?php
/**
 * User: kpistech2
 * Date: 2019-01-28
 * Time: 00:04
 */

namespace App\V1\Home\Transformers\Price;


use App\Price;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class PriceTransformer extends TransformerAbstract
{
    public function transform(Price $price)
    {
        try {
            return [
                'id'    => $price->id,
                'code'  => $price->code,
                'name'  => $price->name,
                'from'  => $price->from,
                'to'    => $price->to,

                'group_id'    => $price->group_id,
                'group_name'  => object_get($price, "customerGroup.name"),
                'description' => $price->description,
                'is_show'     => $price->is_show,
                'is_active'  => $price->is_active,
                'updated_at' => !empty($price->updated_at) ? date('d/m/Y H:i', strtotime($price->updated_at)) : null,
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
