<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:51 PM
 */

namespace App\V1\Home\Models;


use App\Product;

/**
 * Class ProductModel
 * @package App\V1\Home\Models
 */
class ProductModel extends AbstractModel
{
    public function __construct(Product $model = null)
    {
        parent::__construct($model);
    }
}
