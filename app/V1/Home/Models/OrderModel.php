<?php
/**
 * User: kpistech2
 * Date: 2019-01-12
 * Time: 22:24
 */

namespace App\V1\Home\Models;


use App\CUS;
use App\Customer;
use App\Order;
use App\OrderDetail;
use App\PriceDetail;
use App\OFFICE;
use App\Supports\Message;
use App\User;
use Illuminate\Support\Facades\DB;

class OrderModel extends AbstractModel
{
    public function __construct(Order $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        if (empty($input['code'])) {
            $input['code'] = strtoupper(uniqid());
        }
        $customer = Customer::find($input['customer_id']);
        if (!empty($input['receiver'])) {
            $receiver = Customer::find($input['receiver']);
        }
        if (empty($receiver)) {
            $receiver = $customer;
        }
        $seller = [];
        if (!empty($input['seller_id'])) {
            $seller = User::find($input['seller_id']);
        }

        DB::beginTransaction();

        $id = !empty($input['id']) ? $input['id'] : 0;

        if ($id) {
            $order = Order::model()->where('id', $id)->where('seller_id', CUS::getCurrentCustomerId())->first();
            if (empty($order)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }

            if (!in_array($order->status, [ORDER_STATUS_PENDING, ORDER_STATUS_APPROVED, ORDER_STATUS_SHIPPED])) {
                throw new \Exception(Message::get("orders.update-block", "ID: #" . $order->code));
            }

            $order->customer_id = array_get($input, 'customer_id', $order->customer_id);
            $order->customer_name = array_get($input, 'customer_name', $order->customer_name);
            $order->customer_address = array_get($input, 'customer_address', $order->customer_address);
            $order->customer_phone = array_get($input, 'customer_phone', $order->customer_phone);
            $order->is_active = array_get($input, 'is_active', $order->is_active);
            $order->receiver_id = array_get($input, 'receiver_id', $order->receiver_id);
            $order->receiver_name = array_get($input, 'receiver_name', $order->receiver_name);
            $order->receiver_address = array_get($input, 'receiver_address', $order->receiver_address);
            $order->receiver_phone = array_get($input, 'receiver_phone', $order->receiver_phone);
            $order->seller_id = array_get($input, 'seller_id', $order->seller_id);
            $order->seller_name = array_get($input, 'seller_name', $order->seller_name);
            $order->seller_phone = array_get($input, 'seller_phone', $order->seller_phone);
            $order->pay_later = array_get($input, 'pay_later', 0);
            $order->price_id = array_get($input, 'price_id', $order->price_id);
            $order->description = array_get($input, 'description', $order->description);
            $order->updated_at = date("Y-m-d H:i:s", time());
            $order->updated_by = CUS::getCurrentCustomerType() . ": #" . CUS::getCurrentCustomerId();
            $order->save();
        } else {
            $param = [
                'code'             => $input['code'],
                'order_date'       => array_get($input, 'order_date', date("Y-m-d H:i:s", time())),
                'price_id'         => $input['price_id'],
                'pay_later'        => array_get($input, 'pay_later', 0),
                'customer_id'      => $input['customer_id'],
                'customer_name'    => array_get($input, 'customer_name',
                    array_get($customer, 'profile.full_name', "NA")),
                'customer_address' => array_get($input, 'customer_address',
                    array_get($customer, 'profile.address', "NA")),
                'customer_phone'   => array_get($input, 'customer_phone', array_get($customer, 'phone', "NA")),
                'is_active'        => array_get($input, 'is_active', 1),
                'receiver_id'      => array_get($input, 'receiver_id', $input['customer_id']),
                'receiver_name'    => array_get($input, 'receiver_name',
                    array_get($receiver, 'profile.full_name', "NA")),
                'receiver_address' => array_get($input, 'receiver_address',
                    array_get($receiver, 'profile.address', "NA")),
                'receiver_phone'   => array_get($input, 'receiver_phone', array_get($receiver, 'phone', "NA")),
                'seller_id'        => array_get($input, 'seller_id', $input['seller_id']),
                'seller_name'      => array_get($input, 'seller_name', array_get($seller, 'profile.full_name', "NA")),
                'seller_phone'     => array_get($input, 'seller_phone', array_get($seller, 'phone', "NA")),
                'total_price'      => 0,
                'status'           => ORDER_STATUS_PENDING
            ];
            $order = $this->create($param);
        }

        // Get Price Table
        $prices = PriceDetail::model()->where('price_id', $order->price_id)->get()->toArray();
        $prices = array_pluck($prices, 'price', 'product_id');

        // Create|Update Order Detail
        $allOrderDetail = OrderDetail::model()->where('order_id', $order->id)->get()->toArray();
        $allOrderDetail = array_pluck($allOrderDetail, 'id', 'product_id');
        $allOrderDetailDelete = $allOrderDetail;
        foreach ($input['details'] as $detail) {
            $price = !isset($detail['price']) || $detail['price'] === '' ? (empty($prices[$detail['product_id']]) ? 0 : $prices[$detail['product_id']]) : $detail['price'];

            if (empty($allOrderDetail[$detail['product_id']])) {
                // Create Detail
                $orderDetail = new OrderDetailModel();
                $orderDetail->create([
                    'order_id'     => $order->id,
                    'product_id'   => $detail['product_id'],
                    'warehouse_id' => $detail['warehouse_id'],
                    'qty'          => $detail['qty'],
                    'price'        => $price,
                    'description'  => array_get($detail, 'description'),
                    'decrement'    => !empty($detail['decrement']) && $detail['decrement'] == true ? 1 : 0,
                    'is_active'    => 1,
                ]);
                continue;
            }
            // Update
            unset($allOrderDetailDelete[$detail['product_id']]);
            /** @var OrderDetail $orderDetail */
            $orderDetail = OrderDetail::find($allOrderDetail[$detail['product_id']]);
            $orderDetail->warehouse_id = $detail['warehouse_id'];
            $orderDetail->qty = $detail['qty'];
            $orderDetail->price = $price;
            $orderDetail->description = array_get($detail, 'description');
            $orderDetail->decrement = !empty($detail['decrement']) && $detail['decrement'] == true ? 1 : 0;
            $orderDetail->updated_at = date('Y-m-d H:i:s', time());
            $orderDetail->updated_by = OFFICE::getCurrentUserId();
            $orderDetail->save();
        }

        // Delete Order Detail
        OrderDetail::model()->whereIn('id', array_values($allOrderDetailDelete))->delete();

        DB::commit();

        return $order;
    }

    public function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        $this->sortBuilder($query, $input);

        $query->where('customer_id', $input['customer_id'])
            ->orWhere('seller_id', CUS::getCurrentCustomerId())
            ->orWhere('created_by', 'USER: #' . CUS::getCurrentCustomerId())
            ->orWhere('updated_by', 'USER: #' . CUS::getCurrentCustomerId());

        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }
}
