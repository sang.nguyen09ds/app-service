<?php
/**
 * User: Administrator
 * Date: 30/12/2018
 * Time: 12:08 AM
 */

namespace App\V1\Home\Models;


class CategoryModel extends AbstractModel
{
    /**
     * CategoryModel constructor.
     * @param Category|null $model
     */
    public function __construct(Category $model = null)
    {
        parent::__construct($model);
    }

    public function getAll($input = [], $with = [])
    {
        $query = $this->make($with)
            ->orderBy('parent_id')
            ->orderBy('id');

        $data = $query->select([
            'id', 'code', 'name', 'description', 'parent_id'
        ])->get()->toArray();
        $result = [];
        if (!empty($data)) {
            foreach ($data as $key => $root) {
                if (!empty($root['parent_id'])) {
                    break;
                }
                unset($data[$key]);
                $sub['sub_category'] = $this->loadSub($data, $root['id']);
                $result[] = array_merge($root, $sub);
            }
        }

        return $result;
    }

    public function show($id, $with = [])
    {
        $query = $this->make($with)
            ->orderBy('parent_id')
            ->orderBy('id');

        $data = $query->select([
            'id', 'code', 'name', 'description', 'parent_id'
        ])->get()->toArray();
        $result = [];
        if (!empty($data)) {
            foreach ($data as $key => $root) {
                if ($root['id'] == $id) {
                    unset($data[$key]);
                    $sub['sub_category'] = $this->loadSub($data, $root['id']);
                    $result[] = array_merge($root, $sub);
                }
            }
        }

        return $result;
    }

    private function loadSub(&$data, $parent_id)
    {
        $result = [];
        foreach ($data as $key => $item) {
            if ($item['parent_id'] == $parent_id) {
                unset($data[$key]);
                $sub['sub_category'] = $this->loadSub($data, $item['id']);
                $result[] = array_merge($item, $sub);
            }
        }

        return $result;
    }
}