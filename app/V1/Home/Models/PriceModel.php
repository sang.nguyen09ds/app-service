<?php
/**
 * User: kpistech2
 * Date: 2019-01-28
 * Time: 00:02
 */

namespace App\V1\Home\Models;


use App\Price;

class PriceModel extends AbstractModel
{
    /**
     * PriceModel constructor.
     *
     * @param Price|null $model
     */
    public function __construct(Price $model = null)
    {
        parent::__construct($model);
    }
}
