<?php
/**
 * User: kpistech2
 * Date: 2019-01-27
 * Time: 23:31
 */

namespace App\V1\Home\Models;


use App\Warehouse;

class WarehouseModel extends AbstractModel
{
    public function __construct(Warehouse $model = null)
    {
        parent::__construct($model);
    }
}
