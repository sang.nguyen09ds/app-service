<?php
/**
 * User: kpistech2
 * Date: 2019-01-13
 * Time: 00:17
 */

namespace App\V1\Home\Models;


use App\Cart;
use App\CartDetail;
use App\CUS;
use App\Customer;
use App\Supports\Message;
use Illuminate\Support\Facades\DB;

class CartModel extends AbstractModel
{
    public function __construct(Cart $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;

        DB::beginTransaction();
        if ($id) {
            $cart = Cart::model()->where('customer_id', CUS::getCurrentCustomerId())->where('id', $id)->first();
            if (empty($cart)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $cart->updated_at = date("Y-m-d H:i:s", time());
            $cart->updated_by = CUS::getCurrentCustomerId();
            $cart->save();
        } else {
            $code = strtoupper(uniqid());
            $param = [
                'code'        => $code,
                'name'        => $code,
                'customer_id' => CUS::getCurrentCustomerId(),
                'is_active'   => 1,
            ];
            $cart = $this->create($param);
        }

        // Create|Update Cart Detail
        $allCartDetail = CartDetail::model()->where('cart_id', $cart->id)->get()->toArray();
        $allCartDetail = array_pluck($allCartDetail, 'id', 'product_id');
        $allCartDetailDelete = $allCartDetail;
        foreach ($input['details'] as $detail) {
            if (empty($allCartDetail[$detail['product_id']])) {
                // Create Detail
                $cartDetail = new CartDetailModel();
                $cartDetail->create([
                    'cart_id'    => $cart->id,
                    'product_id' => $detail['product_id'],
                    'qty'        => $detail['qty'],
                    'is_active'  => 1,
                ]);
                continue;
            }
            // Update
            unset($allCartDetailDelete[$detail['product_id']]);
            /** @var CartDetail $cartDetail */
            $cartDetail = CartDetail::find($allCartDetail[$detail['product_id']]);
            $cartDetail->qty = $detail['qty'];
            $cartDetail->updated_at = date('Y-m-d H:i:s', time());
            $cartDetail->updated_by = CUS::getCurrentCustomerId();
            $cartDetail->save();
        }

        // Delete Cart Detail
        CartDetail::model()->whereIn('id', array_keys($allCartDetailDelete))->delete();

        DB::commit();

        return $cart;
    }
}