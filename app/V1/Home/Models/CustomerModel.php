<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:51 PM
 */

namespace App\V1\Home\Models;


use App\Customer;
use App\CustomerProfile;
use App\Supports\Message;
use Illuminate\Support\Facades\DB;

class CustomerModel extends AbstractModel
{
    public function __construct(Customer $model = null)
    {
        parent::__construct($model);
    }
}