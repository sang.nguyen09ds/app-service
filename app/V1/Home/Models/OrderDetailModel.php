<?php
/**
 * User: kpistech2
 * Date: 2019-01-12
 * Time: 22:24
 */

namespace App\V1\Home\Models;


use App\OrderDetail;

class OrderDetailModel extends AbstractModel
{
    /**
     * OrderDetailModel constructor.
     *
     * @param OrderDetail|null $model
     */
    public function __construct(OrderDetail $model = null)
    {
        parent::__construct($model);
    }
}
