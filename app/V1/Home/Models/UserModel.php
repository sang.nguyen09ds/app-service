<?php
/**
 * User: kpistech2
 * Date: 2019-01-26
 * Time: 22:55
 */

namespace App\V1\Home\Models;


use App\User;

class UserModel extends AbstractModel
{
    /**
     * UserModel constructor.
     * @param User|null $model
     */
    public function __construct(User $model = null)
    {
        parent::__construct($model);
    }
}