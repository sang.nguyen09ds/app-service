<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 01/01/2019
 * Time: 10:15 PM
 */

namespace App\V1\CMS\Validators;


use App\Customer;
use App\CustomerType;
use App\Http\Validators\ValidatorBase;
use App\Supports\Message;

class CustomerTypeUpdateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'code'  => [
                'nullable',
                'max:20',
                function ($attribute, $value, $fail) {
                    if (!empty($value)) {
                        $customerType = CustomerType::model()->where('code', $value)->get();
                        if (count($customerType) > 1) {
                            return $fail(Message::get("unique", "$attribute: #$value"));
                        }
                    }
                    return true;
                }
            ],
            'name'  => 'nullable|max:50',
            'point' => [
                'nullable',
                'max:20',
                function ($attribute, $value, $fail) {
                    if (!empty($value)) {
                        if (!is_numeric($value) || $value < 0) {
                            return $fail(Message::get("V003", "$attribute: #$value"));
                        }
                    }
                    return true;
                }
            ]
        ];
    }

    protected function attributes()
    {
        return [
            'code'  => Message::get("code"),
            'name'  => Message::get("alternative_name"),
            'point' => Message::get("point"),
        ];
    }
}
