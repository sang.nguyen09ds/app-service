<?php


namespace App\V1\CMS\Validators;


use App\Folder;
use App\Http\Validators\ValidatorBase;
use App\Supports\Message;
use Illuminate\Http\Request;

class FolderUpdateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id' => 'nullable|exists:folders,id,deleted_at,NULL',
        ];
    }

    protected function attributes()
    {
        return [
            'id' => Message::get("id"),
        ];
    }
}