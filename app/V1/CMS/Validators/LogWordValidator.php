<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 5/22/2019
 * Time: 12:42 AM
 */

namespace App\V1\CMS\Validators;


use App\Http\Validators\ValidatorBase;
use App\Supports\Message;
use Illuminate\Http\Request;

class LogWordValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'issue_id'     => 'required|exists:issues,id,deleted_at,NULL',
            'time_spent'   => 'required',
            'date_started' => 'nullable|date_format:d-m-Y H:i',
        ];
    }

    protected function attributes()
    {
        return [
            'issue_id'     => Message::get("issue_id"),
            'time_spent'   => Message::get("time_spent"),
            'date_started' => Message::get("date_started"),
        ];
    }

}