<?php
/**
 * Created by PhpStorm.
 * User: SANG NGUYEN
 * Date: 2/28/2019
 * Time: 11:12 AM
 */

namespace App\V1\CMS\Validators;


use App\Http\Validators\ValidatorBase;
use App\Product;
use App\Supports\Message;
use Illuminate\Http\Request;

class ProductCreateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id'                => 'exists:products,id,deleted_at,NULL',
            'name'              => 'required|max:100',
            'code'              => 'required|string|max:50|unique:products,code',
            'categories_id'     => 'required',
            'short_description' => 'max:300',
            'unit'              => 'max:20',
            'tax'               => 'numeric',
            'image'             => 'max:150',
            'is_active'         => 'integer|max:1',
            'color'             => 'max:7',
            'qty_remain'        => 'numeric',
        ];
    }

    protected function attributes()
    {
        return [
            'name'              => Message::get("name"),
            'code'              => Message::get("code"),
            'categories_id'     => Message::get("categories_id"),
            'short_description' => Message::get("short_description"),
            'unit'              => Message::get("unit"),
            'tax'               => Message::get("tax"),
            'image'             => Message::get("image"),
            'color'             => Message::get("color"),
            'qty_remain'        => Message::get("qty"),
        ];
    }
}