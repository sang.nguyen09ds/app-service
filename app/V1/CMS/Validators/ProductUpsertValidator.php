<?php

namespace App\V1\CMS\Validators;

use App\Http\Validators\ValidatorBase;
use App\Product;
use App\Supports\Message;
use Illuminate\Http\Request;
/**
 * Class ProductUpsertValidator
 *
 * @package App\V1\CMS\Validators
 */
class ProductUpsertValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id'                => 'required|exists:products,id,deleted_at,NULL',
            'name'              => 'required|max:100',
            'code'              => [
                'required',
                'max:50',
                function ($attribute, $value, $fail) {
                    $input = Request::capture();
                    $item = Product::where('code', $value)->whereNull('deleted_at')->get()->toArray();
                    if (!empty($item) && count($item) > 0) {
                        if (count($item) > 1 || ($input['id'] > 0 && $item[0]['id'] != $input['id'])) {
                            return $fail(Message::get("unique", "$attribute: #$value"));
                        }
                    }
                }
            ],
            'categories_id'     => 'required',
            'short_description' => 'max:300',
            'unit'              => 'max:20',
            'tax'               => 'numeric',
            'image'             => 'max:150',
            'is_active'         => 'integer|max:1',
            'color'             => 'max:7',
            'qty_remain'        => 'numeric',
        ];
    }

    protected function attributes()
    {
        return [
            'name'              => Message::get("name"),
            'code'              => Message::get("code"),
            'categories_id'     => Message::get("categories_id"),
            'short_description' => Message::get("short_description"),
            'unit'              => Message::get("unit"),
            'tax'               => Message::get("tax"),
            'image'             => Message::get("image"),
            'color'             => Message::get("color"),
            'qty_remain'        => Message::get("qty"),
        ];
    }
}
