<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 4/1/2019
 * Time: 10:12 AM
 */

namespace App\V1\CMS\Validators\DepartmentsValidator;

use App\Departments;
use App\Http\Validators\ValidatorBase;
use App\Supports\Message;


class DepartmentsCreateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'code' => [
                'required',
                'max:100',
                function ($attribute, $value, $fail) {
                    if (!empty($value)) {
                        $item = Departments::model()->where('code', $value)->first();
                        if (!empty($item)) {
                            return $fail(Message::get("unique", "$attribute: #$value"));
                        }
                    }
                    return true;
                }
            ],

            'name' => 'required|max:50',
        ];
    }

    protected function attributes()
    {
        return [
            'code' => Message::get("code"),
            'name' => Message::get("name"),
        ];
    }
}