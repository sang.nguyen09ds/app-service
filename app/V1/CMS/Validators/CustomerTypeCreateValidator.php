<?php
/**
 * User: Administrator
 * Date: 01/01/2019
 * Time: 10:15 PM
 */

namespace App\V1\CMS\Validators;


use App\CustomerType;
use App\Http\Validators\ValidatorBase;
use App\Supports\Message;

class CustomerTypeCreateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'code'  => [
                'required',
                'max:20',
                function ($attribute, $value, $fail) {
                    $customerType = CustomerType::model()->where('code', $value)->first();
                    if (!empty($customerType)) {
                        return $fail(Message::get("unique", "$attribute: #$value"));
                    }
                    return true;
                }
            ],
            'name'  => 'required|max:50',
            'point' => [
                'required',
                'max:20',
                function ($attribute, $value, $fail) {
                    if (!is_numeric($value) || $value < 0) {
                        return $fail(Message::get("V003", "$attribute: #$value"));
                    }
                    return true;
                }
            ],
        ];
    }

    protected function attributes()
    {
        return [
            'code'  => Message::get("code"),
            'name'  => Message::get("alternative_name"),
            'point' => Message::get("point"),
        ];
    }
}
