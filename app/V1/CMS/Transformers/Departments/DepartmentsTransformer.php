<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 4/1/2019
 * Time: 10:02 AM
 */

namespace App\V1\CMS\Transformers\Departments;


use App\Departments;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class DepartmentsTransformer extends TransformerAbstract
{
    public function transform(Departments $departments)
    {
        try {
            return [
                'id'          => $departments->id,
                'code'        => $departments->code,
                'name'        => $departments->name,
                'description' => $departments->description,
                'is_active'   => $departments->is_active,
                'created_at'  => date('d/m/Y H:i', strtotime($departments->created_at)),
                'updated_at'  => date('d/m/Y H:i', strtotime($departments->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}