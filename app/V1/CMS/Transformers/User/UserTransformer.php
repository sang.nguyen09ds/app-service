<?php
/**
 * User: Dai Ho
 * Date: 22-Mar-17
 * Time: 23:43
 */

namespace App\V1\CMS\Transformers\User;

use App\Supports\OFFICE_Error;
use App\User;
use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 *
 * @package App\V1\CMS\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        try {
            $avatar = !empty($user->profile->avatar) ? url('/v0') . "/img/" . $user->profile->avatar : null;
            $birthday = object_get($user, 'profile.birthday', null);
            return [
                'id'              => $user->id,
                'code'            => $user->code,
                'phone'           => $user->phone,
                'email'           => $user->email,
                'username'        => $user->username,
                'department_id'   => $user->department_id,
                'department_name' => object_get($user, "department.name"),
                'department_code' => object_get($user, "department.code"),
                'first_name'      => object_get($user, "profile.first_name", null),
                'last_name'       => object_get($user, "profile.last_name", null),
                'full_name'       => object_get($user, "profile.full_name", null),
                'address'         => object_get($user, "profile.address", null),
                'birthday'        => !empty($birthday) ? date('d/m/Y', strtotime($birthday)) : null,
                'genre'           => object_get($user, "profile.genre", "O"),
                'genre_name'      => config('constants.STATUS.GENRE')
                [strtoupper(object_get($user, "profile.genre", 'O'))],
                'avatar'          => $avatar,
                'id_number'       => object_get($user, "profile.id_number", null),
                'is_active'       => $user->is_active,
                'role_id'         => $user->role_id,
                'created_at'      => date('d/m/Y H:i', strtotime($user->created_at)),
                'updated_at'      => date('d/m/Y H:i', strtotime($user->updated_at)),
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}
