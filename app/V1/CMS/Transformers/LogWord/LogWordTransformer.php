<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 5/22/2019
 * Time: 12:32 AM
 */

namespace App\V1\CMS\Transformers\LogWord;


use App\LogWord;
use App\Supports\OFFICE_Error;
use League\Fractal\TransformerAbstract;

class LogWordTransformer extends TransformerAbstract
{
    public function transform(LogWord $logWord)
    {
        try {
            return [
                'id'               => $logWord->id,
                'issue_id'         => $logWord->issue_id,
                'issue_name'       => object_get($logWord, 'issue.name', null),
                'time_spent'       => $logWord->time_spent,
                'date_started'     => date('d/m/Y H:i',
                    strtotime($logWord->date_started)),
                'work_description' => $logWord->work_description,
                'is_active'        => $logWord->is_active,
                'created_at'       => !empty($logWord->created_at) ? date('d/m/Y H:i',
                    strtotime($logWord->created_at)) : null,
                'created_by'       => object_get($logWord, 'createdBy.profile.full_name'),
                'updated_at'       => !empty($logWord->updated_at) ? date('d/m/Y H:i',
                    strtotime($logWord->updated_at)) : null,
            ];
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            throw new \Exception($response['message'], $response['code']);
        }
    }
}