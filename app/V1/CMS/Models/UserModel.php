<?php
/**
 * User: Administrator
 * Date: 28/09/2018
 * Time: 09:32 PM
 */

namespace App\V1\CMS\Models;

use App\Profile;
use App\Supports\Message;
use App\User;
use Illuminate\Support\Facades\DB;
use App\OFFICE;

class UserModel extends AbstractModel
{
    public function __construct(User $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $phone = "";
        if (!empty($input['phone'])) {
            $phone = str_replace(" ", "", $input['phone']);
            $phone = preg_replace('/\D/', '', $phone);
        }
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $user = User::find($id);
            // username unique
            $this->checkUnique(['code' => $input['code']], $id);

            // code unique
            if (!empty($input['email'])) {
                $this->checkUnique(['email' => $input['email']], $id);
            }
            if (empty($user)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            if (!empty($input['password'])) {
                $password = password_hash($input['password'], PASSWORD_BCRYPT);
            }
            $user->phone = array_get($input, 'phone', $user->phone);
            $user->code = array_get($input, 'code', $user->code);
            $user->department_id = array_get($input, 'department_id', $user->department_id);
            $user->username = array_get($input, 'code', $user->code);
            $user->email = array_get($input, 'email', $user->email);
            $user->role_id = array_get($input, 'role_id', $user->role_id);
            $user->password = empty($password) ? array_get($input, 'password', $user->password) : $password;
            $user->verify_code = array_get($input, 'verify_code', $user->verify_code);
            $user->expired_code = array_get($input, 'expired_code', $user->expired_code);
            $user->is_active = array_get($input, 'is_active', $user->is_active);
            $user->updated_at = date("Y-m-d H:i:s", time());
            $user->updated_by = OFFICE::getCurrentUserId();
            $user->save();
            $profile = Profile::where(['user_id' => $user->id])->first();
            $profile->phone = $user->phone;
            $profile->email = $user->email;
            $profile->save();
        } else {

            $now = date("Y-m-d H:i:s", time());
            $param = [
                'phone'         => $phone,
                'code'          => $input['code'],
                'department_id' => array_get($input, 'department_id'),
                'username'      => OFFICE::array_get($input, 'username', $input['code']),
                'email'         => array_get($input, 'email'),
                'role_id'       => array_get($input, 'role_id'),
                'verify_code'   => mt_rand(100000, 999999),
                'expired_code'  => date('Y-m-d H:i:s', strtotime("+5 minutes")),
                'is_active'     => array_get($input, 'is_active', 1),
                'created_at'    => $now,
                'created_by'    => OFFICE::getCurrentUserId(),
            ];
            if (!empty($input['password'])) {
                $param['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
            }
            $user = $this->create($param);
            $profileModel = new ProfileModel();

            $y = date('Y', time());
            $m = date("m", time());
            $d = date("d", time());
            $dir = !empty($input['avatar']) ? "$y/$m/$d" : null;
            $file_name = empty($dir) ? null : "avatar_{$input['phone']}";
            if ($file_name) {
                $avatars = explode("base64,", $input['avatar']);
                $input['avatar'] = $avatars[1];
                if (!empty($file_name) && !is_image($avatars[1])) {
                    return $this->response->errorBadRequest(Message::get("V002", "Avatar"));
                }
            }
            $names = explode(" ", trim($input['full_name']));

            $first = $names[0];
            unset($names[0]);
            $last = !empty($names) ? implode(" ", $names) : null;

            $prProfile = [
                'email'      => array_get($input, 'email'),
                'is_active'  => 1,
                'first_name' => $first,
                'last_name'  => $last,
                'full_name'  => $input['full_name'],
                'address'    => array_get($input, 'address', null),
                'phone'      => array_get($input, 'phone', null),
                'birthday'   => empty($input['birthday']) ? null : $input['birthday'],
                'genre'      => array_get($input, 'genre', "O"),
                'avatar'     => $file_name ? $dir . "/" . $file_name . ".jpg" : null,
                'id_number'  => array_get($input, 'id_number', 0),
                'user_id'    => $user->id,
            ];
            $profileModel->create($prProfile);
        }

        // DB::beginTransaction();

//        $id = !empty($input['id']) ? $input['id'] : 0;
//        if ($id) {
//            // Update User
//            $param['id'] = $id;
//            $user = $this->update($param);
//        } else {
//            // Create User
//            $user = $this->create($param);
//        }
//        $profile = Profile::where(['user_id' => $user->id])->first();
//        // Create Profile
//        $profileModel = new ProfileModel();
//        if (empty($profile)) {
//            $profileModel->create($prProfile);
//        } else {
//            $prProfile['id'] = $profile->id;
//            $profileModel->update($prProfile);
//        }
//

        //DB::commit();

        return $user;
    }

    public
    function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $this->sortBuilder($query, $input);
        if (!empty($input['code'])) {
            $query = $query->where('code', 'like', "%{$input['code']}%");
        }
        if (!empty($input['phone'])) {
            $query = $query->where('phone', 'like', "%{$input['phone']}%");
        }
        if (!empty($input['full_name'])) {
            $query = $query->whereHas('profile', function ($q) use ($input) {
                $q->where('full_name', 'like', "%{$input['full_name']}%");
            });
        }
        if (!empty($input['department_id'])) {
            $query = $query->where('department_id', 'like', "%{$input['department_id']}%");
        }

        if (isset($input['is_active'])) {
            $query = $query->where('is_active', 'like', "%{$input['is_active']}%");
        }

        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }

    public function searchKDrive($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $this->sortBuilder($query, $input);
        if (!empty($input['code'])) {
            $query = $query->where('code', 'like', "%{$input['code']}%");
        }
        if (!empty($input['phone'])) {
            $query = $query->where('phone', 'like', "%{$input['phone']}%");
        }
        if (!empty($input['full_name'])) {
            $query = $query->whereHas('profile', function ($q) use ($input) {
                $q->where('full_name', 'like', "%{$input['full_name']}%");
            });
        }
        if (!empty($input['department_id'])) {
            $query = $query->where('department_id', 'like', "%{$input['department_id']}%");
        }

        if (isset($input['is_active'])) {
            $query = $query->where('is_active', 'like', "%{$input['is_active']}%");
        }
        $query = $query->where('id', '!=', OFFICE::getCurrentUserId());
        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }
}
