<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/20/2019
 * Time: 3:12 PM
 */

namespace App\V1\CMS\Models;


use App\OFFICE;
use App\ProductApp;
use App\SizeColor;
use App\Supports\Message;

class ProductAppModel extends AbstractModel
{
    public function __construct(ProductApp $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input, $request)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;

        if ($id) {
            $product = ProductApp::find($id);
            if (empty($product)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $product->name = array_get($input, 'name', $product->name);
            $product->title = array_get($input, 'title', $product->title);
            $product->size_id = array_get($input, 'size_id', $product->size_id);
            $product->color_id = array_get($input, 'color_id', $product->color_id);
            $product->categories_id = array_get($input, 'categories_id', $product->categories_id);
            $product->description = array_get($input, 'description', $product->description);
            $product->is_active = array_get($input, 'is_active', $product->is_active);
            $product->updated_at = date("Y-m-d H:i:s", time());
            $product->updated_by = OFFICE::getCurrentUserId();
            $product->save();
        } else {

            if ($request->hasFile('file')) {

                $file = $request->file('file');

                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                $fileNameToStore = $this->removeAccented($fileNameToStore);
                $fileNameToStore = str_replace(' ', "", $fileNameToStore);
                $filePath = realpath(public_path('uploads'));
                $file->move($filePath, $fileNameToStore);
                $image= 'public/uploads/'.$fileNameToStore;
                dd($image);
            } else {
                return response()->json(['data' => "Upload Error"]);
            }
            $param = [
                'name'          => array_get($input, 'name', NULL),
                'title'         => array_get($input, 'title', NULL),
                'image'         => $fileNameToStore,
                'color_id'      => array_get($input, 'color_id', NULL),
                'categories_id' => array_get($input, 'categories_id', NULL),
                'description'   => array_get($input, 'description', NULL),
                'is_active'     => 1,
            ];
            $product = $this->create($param);
        }

        // Create|Update Multiple thumbnail or Size Color
        $allThumbnail = SizeColor::model()->where('product_id', $product->id)->get()->toArray();
        $allThumbnail = array_pluck($allThumbnail, 'product_id', 'id');
        $allThumbnailDelete = $allThumbnail;
        foreach ($input['details'] as $detail) {
            if (empty($allThumbnailDelete[$detail['id']])) {

                // Create Detail
                $imageDetail = new SizeColor();
                $imageDetail->create([
                    'product_id' => $product->id,
                    'size_id'    => array_get($detail, 'size_id'),
                    'color_id'   => array_get($detail, 'color_id'),
                    'thumbnail'  => array_get($detail, 'description'),
                    'is_active'  => 1,
                ]);
                continue;
            }
            // Update
            /**
             *
             */
            unset($allThumbnailDelete[$detail['id']]);
            $thumbnailDetail = SizeColor::find($detail['id']);
            $thumbnailDetail->product_id = !empty($detail['product_id']) ? $detail['product_id'] : null;
            $thumbnailDetail->size_id = array_get($detail, 'size_id');
            $thumbnailDetail->color_id = array_get($detail, 'color_id');
            $thumbnailDetail->thumbnail = array_get($detail, 'thumbnail');
            $thumbnailDetail->updated_at = date('Y-m-d H:i:s', time());
            $thumbnailDetail->updated_by = OFFICE::getCurrentUserId();
            $thumbnailDetail->save();
        }
        // Delete Order Detail
        SizeColor::model()->whereIn('id', array_values($allThumbnailDelete))->delete();

        return $product;
    }
}
