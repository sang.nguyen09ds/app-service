<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:45 PM
 */

namespace App\V1\CMS\Models;


use App\OFFICE;
use App\Size;
use App\Supports\Message;

class SizeModel extends AbstractModel
{
    public function __construct(Size $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $size = Size::find($id);
            if (empty($size)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $size->name = array_get($input, 'name', $size->name);
            $size->title = array_get($input, 'title', $size->title);
            $size->is_active = array_get($input, 'is_active', $size->is_active);
            $size->updated_at = date("Y-m-d H:i:s", time());
            $size->updated_by = OFFICE::getCurrentUserId();
            $size->save();
        } else {
            $param = [
                'name'      => array_get($input, 'name', NULL),
                'title'     => array_get($input, 'title', NULL),
                'is_active' => 1,
            ];
            $size = $this->create($param);
        }
        return $size;
    }
}
