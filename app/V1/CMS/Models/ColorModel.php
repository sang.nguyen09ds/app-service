<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:43 PM
 */

namespace App\V1\CMS\Models;


use App\Color;
use App\OFFICE;
use App\Supports\Message;

class ColorModel extends AbstractModel
{
    public function __construct(Color $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $color = Color::find($id);
            if (empty($size)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $color->name = array_get($input, 'name', $color->name);
            $color->code = array_get($input, 'code', $color->code);
            $color->title = array_get($input, 'title', $color->title);
            $color->is_active = array_get($input, 'is_active', $color->is_active);
            $color->updated_at = date("Y-m-d H:i:s", time());
            $color->updated_by = OFFICE::getCurrentUserId();
            $color->save();
        } else {
            $param = [
                'name'         => array_get($input, 'name', NULL),
                'hex_code'     => '#' . array_get($input, 'hex_code', NULL),
                'decimal_code' => array_get($input, 'decimal_code', NULL),
                'title'        => array_get($input, 'title', NULL),
                'is_active'    => 1,
            ];
            $color = $this->create($param);
        }
        return $color;
    }
}
