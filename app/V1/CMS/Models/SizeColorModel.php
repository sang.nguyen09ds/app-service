<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/29/2019
 * Time: 7:22 PM
 */

namespace App\V1\CMS\Models;


use App\SizeColor;

class SizeColorModel extends AbstractModel
{
    public function __construct(SizeColor $model = null)
    {
        parent::__construct($model);
    }
}
