<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 4/1/2019
 * Time: 9:56 AM
 */

namespace App\V1\CMS\Models;


use App\Departments;
use App\OFFICE;
use App\Supports\Message;

class DepartmentsModel extends AbstractModel
{
    public function __construct(Departments $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $this->checkUnique(['code' => $input['code']], $id);
            $departments = Departments::find($id);
            if (empty($departments)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $departments->code = array_get($input, 'code', $departments->code);
            $departments->name = array_get($input, 'name', $departments->name);
            $departments->is_active = array_get($input, 'is_active', $departments->is_active);
            $departments->description = array_get($input, 'description', NULL);
            $departments->updated_at = date("Y-m-d H:i:s", time());
            $departments->updated_by = OFFICE::getCurrentUserId();
            $departments->save();
        } else {
            $param = [
                'code'        => $input['code'],
                'name'        => $input['name'],
                'description' => array_get($input, 'description'),
                'is_active'   => 1,

            ];

            $departments = $this->create($param);
        }

        return $departments;
    }
}