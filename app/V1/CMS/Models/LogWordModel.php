<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 5/22/2019
 * Time: 12:10 AM
 */

namespace App\V1\CMS\Models;


use App\LogWord;
use App\OFFICE;
use App\Supports\Message;

class LogWordModel extends AbstractModel
{
    public function __construct(LogWord $model = null)
    {
        parent::__construct($model);
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $logWord = LogWord::find($id);
            if (empty($logWord)) {
                throw new \Exception(Message::get("V003", "ID: #$id"));
            }
            $logWord->issue_id = array_get($input, 'issue_id', $logWord->name);
            $logWord->time_spent = array_get($input, 'time_spent', $logWord->code);
            $logWord->date_started = date("Y-m-d H:i", strtotime(array_get($input, 'date_started', $logWord->date_started)));
            $logWord->work_description = array_get($input, 'work_description', $logWord->work_description);
            $logWord->is_active = array_get($input, 'is_active', $logWord->is_active);
            $logWord->updated_at = date("Y-m-d H:i:s", time());
            $logWord->updated_by = OFFICE::getCurrentUserId();
            $logWord->save();
        } else {
            $param = [
                'issue_id'         => $input['issue_id'],
                'time_spent'       => array_get($input, 'time_spent', NULL),
                'date_started'     => !empty($input['date_started']) ? date("Y-m-d H:i", strtotime($input['date_started'])) : null,
                'work_description' => array_get($input, 'work_description', NULL),
                'is_active'        => 1,
            ];

            $logWord = $this->create($param);
        }

        return $logWord;
    }
}