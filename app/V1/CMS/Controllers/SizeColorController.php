<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/29/2019
 * Time: 7:21 PM
 */

namespace App\V1\CMS\Controllers;


use App\V1\CMS\Models\SizeColorModel;

class SizeColorController extends BaseController
{
    /**
     * @var SizeColorModel
     */
    protected $model;

    /**
     * SizeColorController constructor.
     */
    public function __construct()
    {
        $this->model = new SizeColorModel();
    }
}
