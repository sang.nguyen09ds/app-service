<?php

namespace App\V1\CMS\Controllers;

use App\OFFICE;
use App\Profile;
use App\Supports\Log;
use App\Supports\OFFICE_Email;
use App\Supports\OFFICE_Error;
use App\Supports\Message;
use App\User;
use App\V1\CMS\Models\UserModel;
use App\V1\CMS\Transformers\User\UserCustomerProfileTransformer;
use App\V1\CMS\Transformers\User\UserProfileTransformer;
use App\V1\CMS\Transformers\User\UserTransformer;
use App\V1\CMS\Validators\UserChangePasswordValidator;
use App\V1\CMS\Validators\UserCreateValidator;
use App\V1\CMS\Validators\UserProfileUpdateValidator;
use App\V1\CMS\Validators\UserUpdateValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use League\Flysystem\Config;

/**
 * Class UserController
 *
 * @package App\V1\CMS\Controllers
 */
class UserController extends BaseController
{

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * @param UserTransformer $userTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, UserTransformer $userTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        // Log::view($this->model->getTable());
        return $this->response->paginator($result, $userTransformer);
    }

    public function searchProfile(Request $request, UserCustomerProfileTransformer $userCustomerProfileTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        // Log::view($this->model->getTable());
        $user = $this->model->searchs($input, [], $limit);
        return $this->response->paginator($user, $userCustomerProfileTransformer);
    }

    public function viewProfile(UserCustomerProfileTransformer $userCustomerProfileTransformer)
    {
        try {
            $userId = OFFICE::getCurrentUserId();
            $user = User::find($userId);
            //  Log::view($this->model->getTable());
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }

        return $this->response->item($user, $userCustomerProfileTransformer);
    }


    public function viewProfileUser($id, UserCustomerProfileTransformer $userCustomerProfileTransformer)
    {
        try {
            $user = User::find($id);
            //  Log::view($this->model->getTable());
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }

        return $this->response->item($user, $userCustomerProfileTransformer);
    }


    public function view($id, UserTransformer $userTransformer)
    {
        $user = User::find($id);
        //  Log::view($this->model->getTable());
        return $this->response->item($user, $userTransformer);
    }

    public function info(UserProfileTransformer $userProfileTransformer)
    {

        $id = OFFICE::getCurrentUserId();
        $user = User::find($id);
        //  Log::view($this->model->getTable());
        if (empty($user)) {
            return $this->response->errorBadRequest(Message::get('V003', "ID #$id"));
        }
        return $this->response->item($user, $userProfileTransformer);
    }

    public function getInfo(Request $request, UserTransformer $userTransformer)
    {

        $input = $request->all();
        try {
            $userId = OFFICE::getCurrentUserId();
            $userName = OFFICE::getCurrentUserName();
            $user = $this->model->getFirstBy('id', $userId);
            // Log::view($this->model->getTable());
            $roleCode = OFFICE::getCurrentRoleCode();
            $roleName = OFFICE::getCurrentRoleName();
            // Get Employee
            $result = [
                'user_id'   => $user->id,
                'username'  => $user->username,
                'full_name' => $userName,
                'email'     => $user->email,
                'is_super'  => $user->is_super,
                'role_code' => $roleCode,
                'role_name' => $roleName,
            ];

            // Show Profile
            if (!empty($input['profile']) && $input['profile'] == 1) {
                $result['profile'] = [
                    'first_name' => object_get($user, "profile.first_name", null),
                    'last_name'  => object_get($user, "profile.last_name", null),
                    'email'      => object_get($user, "profile.email", null),
                    'short_name' => object_get($user, "profile.short_name", null),
                    'address'    => object_get($user, "profile.address", null),
                    'phone'      => object_get($user, "profile.phone", null),
                    'language'   => object_get($user, "profile.language", "VI"),
                    'birthday'   => object_get($user, "profile.birthday", null),
                    'genre'      => object_get($user, "profile.genre", null),
                    'genre_name' => Config::get('constants.STATUS.GENRE')
                    [strtoupper(object_get($user, "profile.genre", 'O'))],
                    'avatar'     => !empty($input['img']) && $input['img'] == 1 ? get_image(object_get($user,
                        "profile.avatar", null)) : null,
                ];
            }
            // Show Permissions
            if (!empty($input['permissions']) && $input['permissions'] == 1) {
                $result['permissions'] = OFFICE::getCurrentPermission();
            }

        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }

        return ['status' => Response::HTTP_OK, 'data' => $result];
    }


    public function create(Request $request, UserCreateValidator $userCreateValidator, UserTransformer $userTransformer)
    {
        $input = $request->all();
        $userCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $user = $this->model->upsert($input);
            Log::create($this->model->getTable(), $user->code);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($user, $userTransformer);
    }

    public function update(
        $id,
        Request $request,
        UserUpdateValidator $userUpdateValidator,
        UserTransformer $userTransformer
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $userUpdateValidator->validate($input);

        try {
            DB::beginTransaction();
            $user = $this->model->upsert($input);
            Log::update($this->model->getTable(), $user->code);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }

        return $this->response->item($user, $userTransformer);
    }

    public function updateProfile(Request $request, UserProfileUpdateValidator $userProfileUpdateValidator)
    {
        $input = $request->all();
        $input['user_id'] = OFFICE::getCurrentUserId();
        $userProfileUpdateValidator->validate($input);
        if (empty($input)) {
            return ['status' => Message::get("users.update-success", Message::get("profile"))];
        }
        try {
            DB::beginTransaction();
            $profile = Profile::where('user_id', OFFICE::getCurrentUserId())->whereNull('deleted_at')->first();
            $users = User::find(OFFICE::getCurrentUserId());
            $now = date("Y-m-d H:i:s", time());
            if (empty($profile)) {
                $profile = new Profile();
                $profile->created_at = $now;
                $profile->created_by = $input['user_id'];
            }
            if (empty($users)) {
                $users = new User();
                $users->created_at = $now;
                $users->created_by = $input['user_id'];
            }
            if (!empty($input['full_name'])) {
                $profile->full_name = $input['full_name'];
                $full = explode(" ", $input['full_name']);
                $profile->first_name = trim($full[count($full) - 1]);
                unset($full[count($full) - 1]);
                $profile->last_name = implode(" ", $full);
            }
            if (!empty($input['email'])) {
                $profile->email = $input['email'];
                $users->email = $input['email'];
            }
            if (!empty($input['phone'])) {
                $profile->phone = $input['phone'];
                $users->phone = $input['phone'];
            }
            if (!empty($input['address'])) {
                $profile->address = $input['address'];
            }
            if (!empty($input['birthday'])) {
                $profile->birthday = $input['birthday'];
            }
            if (!empty($input['avatar'])) {
                $avatar = explode(';base64,', $input['avatar']);
                dd($avatar);
                if (!empty($avatar[1])) {
                    $data = base64_decode($avatar[1]);
                    if (!empty($avatar[1])) {
                        file_put_contents(public_path() . "/uploads/avatar/user-" . OFFICE::getCurrentUserId() . ".png",
                            $data);
                        $profile->avatar = "uploads,avatar,user-" . OFFICE::getCurrentUserId() . ".png";
                    }
                }
            }
//            // Change password
//            if (!empty($input['password'])) {
//                $users->password = password_hash($input['password'], PASSWORD_BCRYPT);
//                $users->updated_at = $now;
//                $users->updated_by = OFFICE::getCurrentUserId();
//            }
            $profile->created_at = $now;
            $profile->created_by = $input['user_id'];
            $users->created_at = $now;
            $users->created_by = $input['user_id'];
            $users->save();
            $profile->save();
            Log::update($this->model->getTable(), $profile->full_name);
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("users.update-success", Message::get("profile"))];
    }

    public function changePassword(Request $request, UserChangePasswordValidator $userChangePasswordValidator)
    {
        $input = $request->all();
        $user_id = OFFICE::getCurrentUserId();
        $userChangePasswordValidator->validate($input);
        try {
            DB::beginTransaction();
            $user = User::find($user_id);
            // Change password
            if (!password_verify($input['password'], $user->password)) {
                throw new \Exception(Message::get("V002", Message::get("password")));
            }
            $user->password = password_hash($input['new_password'], PASSWORD_BCRYPT);
            $user->save();

            Log::update($this->model->getTable());
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("users.change-password")];
    }


    public function active($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return $this->response->errorBadRequest(Message::get("users.not-exist", "#$id"));
        }

        try {
            DB::beginTransaction();
            if ($user->is_active === 1) {
                $msgCode = "users.inactive-success";
                $user->is_active = "0";
            } else {
                $user->is_active = "1";
                $msgCode = "users.active-success";
            }
            // Write Log
            Log::update($this->model->getTable(), $user->is_active);
            $user->save();

            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest(Message::get($msgCode, $user->phone));
        }

        return ['status' => Message::get($msgCode, "Product")];
    }

    public function exportUser(Request $request)
    {

        $input = $request->all();
        $result = $this->model->search($input);
        try {
            $date = date('YmdHis', time());
            $dataUser = [
                [
                    'Mã nhân viên',
                    'Họ tên',
                    'Điện thoại',
                    'Email',
                    'Địa chỉ',
                    'Ngày sinh',
                    'Nhóm quyền',
                ],
            ];

            foreach ($result as $user) {
                $birthday = array_get($user, 'profile.birthday', null);
                $dataUser[] = [
                    'code'      => $user['code'],
                    'full_name' => array_get($user, "profile.full_name", null),
                    'phone'     => !empty($user['phone']) ? $user['phone'] : null,
                    'email'     => $user['email'],
                    'address'   => array_get($user, "profile.address", null),
                    'birthday'  => !empty($birthday) ? date('d/m/Y', strtotime($birthday)) : null,
                    'role_name' => array_get($user, "role.name", null),
                ];
            }
            $this->ExcelExport("User_$date", storage_path('Export') . "/User", $dataUser);
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);

            return $this->response->errorBadRequest($response["message"]);
        }
    }

//    public function resetPassword(Request $request)
//    {
//        $input = $request->all();
//        $passwordReset = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'), 1, 8);
//        $userCheck = User::where('username', $input['username'])->where('email', $input['email'])->first();
//        if (empty($userCheck))
//            return $this->response->errorBadRequest(Message::get("users.check-fail"));
//        $param = [
//            'password' => Hash::make($passwordReset)
//        ];
//        $userCheck->update($param);
//        $email = $userCheck->email;
//        $paramSendMail = [
//            'password' => $passwordReset,
//            'username' => $input['username'],
//            'link_to'  => 'http://k-office.kpis.vn/#/login',
//        ];
//        OFFICE_Email::send('mail_send_reset_password', $email, $paramSendMail);
//        return ['status' => Message::get("users.reset-password-success")];
//    }
//    // 'verify_code'   => mt_rand(100000, 999999),
//    //                'expired_code'  => date('Y-m-d H:i:s', strtotime("+5 minutes")),
    public function resetPassword(Request $request)
    {
        $input = $request->all();
        $userCheck = User::where('email', $input['email'])->first();
        $verify_code = mt_rand(100000, 999999);
        $param = [
            'verify_code'  => $verify_code,
            'expired_code' => date('Y-m-d H:i:s', strtotime("+5 minutes")),
        ];
        if (!empty($userCheck)) {
            $userCheck->update($param);

            $paramSendMail = [
                'username'    => $userCheck->username,
                'verify_code' => $verify_code,
            ];

            OFFICE_Email::send('mail_send_reset_password', $userCheck->email, $paramSendMail);
            return ['status' => Message::get("users.reset-password-success")];
        } else {
            return false;
        }
    }

    public function verifyCode(Request $request)
    {
        $input = $request->all();
        $userCheck = User::where('verify_code', $input['verify_code'])
            ->whereTime('expired_code', '>=', date("Y-m-d H:i:s", time()))->first();
        if (!empty($userCheck)) {

            $userCheck->password = password_hash($input['new_password'], PASSWORD_BCRYPT);
            $userCheck->save();
            return ['status' => Message::get("users.reset-password-success")];
        }
    }
    public  function  searchKDrive(Request $request, UserTransformer $userTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->searchKDrive($input, [], $limit);
        // Log::view($this->model->getTable());
        return $this->response->paginator($result, $userTransformer);
        
    }


}
