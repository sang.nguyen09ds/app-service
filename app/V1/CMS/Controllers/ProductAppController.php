<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/20/2019
 * Time: 3:05 PM
 */

namespace App\V1\CMS\Controllers;


use App\ProductApp;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\CMS\Models\ProductAppModel;
use App\V1\CMS\Transformers\ProductApp\ProductAppTransformer;
use App\V1\CMS\Validators\ProductAppValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductAppController extends BaseController
{
    /**
     * @var ProductAppModel
     */
    protected $model;

    /**
     * ProductAppController constructor.
     */
    public function __construct()
    {
        $this->model = new ProductAppModel();
    }

    public function search(Request $request, ProductAppTransformer $productAppTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        return $this->response->paginator($result, $productAppTransformer);
    }

    public function detail($id, ProductAppTransformer $issueTransformer)
    {
        try {
            $result = $this->model->getFirstBy('id', $id);
            //   Log::view($this->model->getTable());
            if (empty($result)) {
                return ["data" => []];
            }
        } catch (\Exception $ex) {
            if (env('APP_ENV') == 'testing') {
                return $this->response->errorBadRequest($ex->getMessage());
            } else {
                return $this->response->errorBadRequest(Message::get("R011"));
            }
        }

        return $this->response->item($result, $issueTransformer);
    }

    public function create(Request $request, ProductAppValidator $productAppValidator, ProductAppTransformer $productAppTransformer)
    {
        $input = $request->all();
      //  $productAppValidator->validate($input);
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input,$request);
            dd($result);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $productAppTransformer);
    }

    public function update(
        $id,
        Request $request,
        ProductAppValidator $productAppValidator,
        ProductAppTransformer $productAppTransformer
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $productAppValidator->validate($input);
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $productAppTransformer);
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $result = ProductApp::find($id);
            if (empty($result)) {
                return $this->response->errorBadRequest(Message::get("V003", "ID #$id"));
            }
            // 1. Delete Issue
            $result->delete();
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("product_ap.delete-success", $result->name)];
    }
}
