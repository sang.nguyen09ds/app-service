<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:43 PM
 */

namespace App\V1\CMS\Controllers;

use App\Supports\OFFICE_Error;
use App\V1\CMS\Models\ColorModel;
use App\V1\CMS\Transformers\Color\ColorTransformers;
use App\V1\CMS\Validators\ColorCreateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ColorController extends BaseController
{
    /**
     * @var ColorModel
     */
    protected $model;

    /**
     * ColorController constructor.
     */
    public function __construct()
    {
        $this->model = new ColorModel();
    }

    public function search(Request $request, ColorTransformers $colorTransformers)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        return $this->response->paginator($result, $colorTransformers);
    }

    public function create(
        Request $request,
        ColorCreateValidator $departmentsCreateValidator,
        ColorTransformers $departmentsTransformer
    )
    {
        $input = $request->all();
        $departmentsCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            //Log::create($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $departmentsTransformer);
    }

    public function update(
        $id,
        Request $request,
        ColorCreateValidator $departmentsUpdateValidator,
        ColorTransformers $colorTransformers
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $departmentsUpdateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $colorTransformers);
    }
}
