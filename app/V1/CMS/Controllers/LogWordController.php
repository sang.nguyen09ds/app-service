<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 5/22/2019
 * Time: 12:07 AM
 */

namespace App\V1\CMS\Controllers;


use App\LogWord;
use App\Supports\Log;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\CMS\Models\LogWordModel;
use App\V1\CMS\Transformers\LogWord\LogWordIssueTransformer;
use App\V1\CMS\Transformers\LogWord\LogWordTransformer;
use App\V1\CMS\Validators\LogWordValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogWordController extends BaseController
{
    protected $model;

    /**
     * LogWordController constructor.
     */

    public function __construct()
    {
        $this->model = new LogWordModel();
    }

    public function search(Request $request, LogWordTransformer $logWordTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        return $this->response->paginator($result, $logWordTransformer);
    }

    public function detail($id, LogWordTransformer $logWordTransformer)
    {
        try {
            $result = $this->model->getFirstBy('id', $id);
            if (empty($result)) {
                return ["data" => []];
            }
        } catch (\Exception $ex) {
            if (env('APP_ENV') == 'testing') {
                return $this->response->errorBadRequest($ex->getMessage());
            } else {
                return $this->response->errorBadRequest(Message::get("R011"));
            }
        }

        return $this->response->item($result, $logWordTransformer);
    }

    public function issueDetail($issue_id, LogWordIssueTransformer $logWordIssueTransformer)
    {
        try {
            $result = LogWord::model()->where('issue_id', $issue_id)->get();
            //  Log::view($this->model->getTable());
            foreach ($result as $value) {
                $avatar = object_get($value, "createdBy.profile.avatar");
                $avatar = !empty($avatar) ? url('/v0') . "/img/" . $avatar : null;
                $resultJson[] = [
                    'id'               => $value->id,
                    'issue_id'         => $value->issue_id,
                    'time_spent'       => $value->time_spent,
                    'date_started'     => $value->date_started,
                    'work_description' => $value->work_description,
                    'is_active'        => $value->is_active,
                    'avatar'           => $avatar,
                    'created_by'       => object_get($value, 'createdBy.profile.full_name'),
                    'updated_at'       => date('d/m/Y H:i', strtotime($value->updated_at)),
                ];
            }
        } catch (\Exception $ex) {
            if (env('APP_ENV') == 'testing') {
                return $this->response->errorBadRequest($ex->getMessage());
            } else {
                return $this->response->errorBadRequest(Message::get("R011"));
            }
        }
        if (empty($resultJson)) {
            return ["data" => []];
        }
        return response()->json(['data' => $resultJson]);
    }

    public
    function create(Request $request, LogWordValidator $logWordValidator, LogWordTransformer $logWordTransformer)
    {
        $input = $request->all();
//        if (strpos($input['time_spent'], 'h')) {
//            $input['time_spent'] = chop($input['time_spent'], 'h');
//        } else {
//            $input['time_spent'] = chop($input['time_spent'], 'm');
//        }
        $logWordValidator->validate($input);
        $logWordValidator->validate($input);
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            Log::create($this->model->getTable(), $input['work_description']);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $logWordTransformer);
    }

    public
    function update(
        $id,
        Request $request,
        LogWordValidator $logWordValidator,
        LogWordTransformer $logWordTransformer
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $logWordValidator->validate($input);
        if (!empty($input['progress'])) {
            if ($input['progress'] < 0 || $input['progress'] > 100) {
                throw new \Exception(Message::get("V017"));
            }
        }

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            Log::update($this->model->getTable(), $result->work_description);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $logWordTransformer);
    }

    public
    function delete($id)
    {
        try {
            DB::beginTransaction();
            $result = LogWord::find($id);
            if (empty($result)) {
                return $this->response->errorBadRequest(Message::get("V003", "ID #$id"));
            }
            // 1. Delete LogWord
            $result->delete();
            Log::delete($this->model->getTable(), $result->work_description);
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("task.delete-success", $result->work_description)];
    }

}