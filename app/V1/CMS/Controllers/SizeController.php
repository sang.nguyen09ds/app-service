<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:43 PM
 */

namespace App\V1\CMS\Controllers;


use App\Supports\OFFICE_Error;
use App\V1\CMS\Models\SizeModel;
use App\V1\CMS\Transformers\Size\SizeTransformer;
use App\V1\CMS\Validators\SizeCreateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SizeController extends BaseController
{
    /**
     * @var SizeModel
     */
    protected $model;

    /**
     * SizeController constructor.
     */
    public function __construct()
    {
        $this->model = new SizeModel();
    }

    public function search(Request $request, SizeTransformer $sizeTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        return $this->response->paginator($result, $sizeTransformer);

    }

    public function create(
        Request $request,
        SizeCreateValidator $sizeCreateValidator,
        SizeTransformer $sizeTransformer
    )
    {
        $input = $request->all();
        $sizeCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            //Log::create($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $sizeTransformer);
    }

    public function update(
        $id,
        Request $request,
        SizeCreateValidator $sizeCreateValidator,
        SizeTransformer $sizeTransformer
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $sizeCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
           // Log::update($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $sizeTransformer);
    }
}
