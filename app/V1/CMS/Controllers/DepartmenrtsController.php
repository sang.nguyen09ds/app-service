<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 4/1/2019
 * Time: 9:47 AM
 */

namespace App\V1\CMS\Controllers;

use App\Departments;
use App\Supports\Log;
use App\Supports\Message;
use App\Supports\OFFICE_Error;
use App\V1\CMS\Models\DepartmentsModel;
use App\V1\CMS\Transformers\Departments\DepartmentsTransformer;
use App\V1\CMS\Validators\DepartmentsValidator\DepartmentsCreateValidator;
use App\V1\CMS\Validators\DepartmentsValidator\DepartmentsUpdateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmenrtsController extends BaseController
{
    public function __construct()
    {
        $this->model = new DepartmentsModel();
    }

    /**
     * @param Request $request
     * @param DepartmentsTransformer $departmentsTransformer
     * @return \Dingo\Api\Http\Response
     */

    public function search(Request $request, DepartmentsTransformer $departmentsTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 20);
        $result = $this->model->search($input, [], $limit);
        //Log::view($this->model->getTable());
        return $this->response->paginator($result, $departmentsTransformer);
    }

    public function detail($id, DepartmentsTransformer $departmentsTransformer)
    {
        try {
            $result = Departments::find($id);
            // Log::view($this->model->getTable());
            if (empty($result)) {
                return ["data" => []];
            }
        } catch (\Exception $ex) {
            if (env('APP_ENV') == 'testing') {
                return $this->response->errorBadRequest($ex->getMessage());
            } else {
                return $this->response->errorBadRequest(Message::get("R011"));
            }
        }
        return $this->response->item($result, $departmentsTransformer);
    }

    public function create(
        Request $request,
        DepartmentsCreateValidator $departmentsCreateValidator,
        DepartmentsTransformer $departmentsTransformer
    )
    {
        $input = $request->all();
        $departmentsCreateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            Log::create($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $departmentsTransformer);
    }

    public function update(
        $id,
        Request $request,
        DepartmentsUpdateValidator $departmentsUpdateValidator,
        DepartmentsTransformer $departmentsTransformer
    )
    {
        $input = $request->all();
        $input['id'] = $id;
        $departmentsUpdateValidator->validate($input);

        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            Log::update($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return $this->response->item($result, $departmentsTransformer);
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $result = Departments::find($id);
            if (empty($result)) {
                return $this->response->errorBadRequest(Message::get("V003", "ID #$id"));
            }
            // 1. Delete Departments
            $result->delete();
            Log::delete($this->model->getTable(), $result->name);
            DB::commit();
        } catch (\Exception $ex) {
            $response = OFFICE_Error::handle($ex);
            return $this->response->errorBadRequest($response['message']);
        }
        return ['status' => Message::get("department.delete-success", $result->code)];
    }
}