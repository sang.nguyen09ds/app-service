<?php
/**
 * User: Administrator
 * Date: 01/01/2019
 * Time: 08:32 PM
 */

namespace App;


class CustomerType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_types';

    protected $fillable = [
        "code",
        "name",
        "point",
        "description",
        "is_active",
        "deleted",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];
}
