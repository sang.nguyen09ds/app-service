<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 4/1/2019
 * Time: 9:51 AM
 */

namespace App;


/**
 * Class Departments
 * @package App
 */

class Departments extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    protected $fillable = [
        'code',
        "name",
        "description",
        "is_active",
        "deleted",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];

    public function user()
    {
        return $this->hasMany(__NAMESPACE__ . '\User', 'department_id', 'id');
    }
}