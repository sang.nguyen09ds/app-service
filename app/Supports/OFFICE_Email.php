<?php
/**
 * User: Administrator
 * Date: 16/10/2018
 * Time: 09:48 PM
 */

namespace App\Supports;


use Illuminate\Support\Facades\Mail;

class OFFICE_Email
{
    static $mail_supporter = "sang.nguyen09ds@gmail.com";
    static $view_approval_request = "mail_send_request_approved";
    static $view_approval_status = "mail_send_approval_status";
    static $view_assign_kpi = "mail_send_assign_kpi";
    static $view_reset_password = "mail_send_reset_password";
    static $view_new_user = "mail_send_new_user";
    static $view_register_confirm = "mail_send_register";
    static $view_report_error = "mail_send_report_error";
    static $view_contact = "mail_send_contact";
    static $view_mail_create_issue = "mail_send_create_issue";

    /**
     * @param $view
     * @param $to
     * @param array $data
     * @param array $cc
     * @param array $bcc
     * @param string $subject
     */
    static function send($view, $to, $data = [], $cc = [], $bcc = [], $subject = "K-OFFICE!")
    {
        $data['logo'] = env('APP_LOGO');
        Mail::send($view, $data, function ($message) use ($to, $subject, $data, $cc, $bcc) {
            $message->to($to);

            if (!empty($cc)) {
                $message->cc($cc);
            }

            $bcc[] = self::$mail_supporter;
//            $bcc[] = "sang.nguyen09ds@gmail.com";

            $message->bcc($bcc);

            $message->subject($subject);
        });
    }
}
