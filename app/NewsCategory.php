<?php
/**
 * User: Dai Ho
 * Date: 22-Mar-17
 * Time: 23:43
 */

namespace App;

/**
 * Class NewsCategory
 * @package App
 */
class NewsCategory extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news_categories';

    public function category()
    {
        return $this->hasOne(__NAMESPACE__ . '\Category', 'id', 'category_id');
    }

    public function country()
    {
        return $this->hasOne(__NAMESPACE__ . '\Country', 'id', 'country_id');
    }

    public function city()
    {
        return $this->hasOne(__NAMESPACE__ . '\City', 'id', 'city_id');
    }

    public function district()
    {
        return $this->hasOne(__NAMESPACE__ . '\District', 'id', 'district_id');
    }

    public function ward()
    {
        return $this->hasOne(__NAMESPACE__ . '\Ward', 'id', 'ward_id');
    }
}
