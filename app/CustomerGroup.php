<?php
/**
 * User: Administrator
 * Date: 01/01/2019
 * Time: 08:32 PM
 */

namespace App;


class CustomerGroup extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_groups';

    protected $fillable = [
        "code",
        "name",
        "description",
        "is_active",
        "deleted",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];

    public function details()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerGroupDetail', 'group_id', 'id');
    }
}
