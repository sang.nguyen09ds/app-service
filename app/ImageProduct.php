<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/20/2019
 * Time: 5:29 PM
 */

namespace App;


class ImageProduct extends BaseModel
{
    protected $table = 'image_product';

    protected $fillable = [
        'name',
        'code',
        'thumbnail',
        'product_id',
        'is_active',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    public function product()
    {
        return $this->hasOne(__NAMESPACE__ . '\ProductApp', 'id', 'product_id');
    }
}
