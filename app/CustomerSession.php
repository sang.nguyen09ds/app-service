<?php
/**
 * User: Administrator
 * Date: 28/12/2018
 * Time: 09:46 PM
 */

namespace App;


class CustomerSession extends BaseModel {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_sessions';
}
