<?php
/**
 * User: Dai Ho
 * Date: 22-Mar-17
 * Time: 23:43
 */

namespace App;

/**
 * Class News
 *
 * @package App
 */
class News extends BaseModel {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    protected $fillable = [
        'title',
        "short_description",
        "description",
        "thumbnail",
        "image",
        "content",
        "published_date",
        "is_approved",
        "is_active",
        "deleted",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];

    public function newsCategory() {
        return $this->hasMany(__NAMESPACE__ . '\NewsCategory', 'news_id', 'id');
    }
}
