<?php
/**
 * User: Administrator
 * Date: 21/12/2018
 * Time: 07:46 PM
 */

namespace App;


use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends BaseModel implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customers';

    protected $fillable = [
        'phone',
        'code',
        'sscid',
        'name',
        'card_name',
        'password',
        'email',
        'verify_code',
        'expired_code',
        'role_id',
        'group_id',
        'note',
        'type',
        'point',
        'used_point',
        'is_seller',
        'is_active',
        'is_agency',
        'deleted',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function profile()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerProfile', 'customer_id', 'id');
    }

    public function group()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerGroup', 'id', 'group_id');
    }

    public function type()
    {
        return $this->hasOne(__NAMESPACE__ . '\CustomerType', 'id', 'type_id');
    }
}
