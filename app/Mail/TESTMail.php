<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 6/15/2019
 * Time: 8:00 PM
 */

namespace App\Mail;


use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
//use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TESTMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct()
    {

    }

    public function build()
    {
        return $this->view('sent-mail-test');
    }
}