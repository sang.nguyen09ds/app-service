<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 11/21/2019
 * Time: 10:37 AM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app'),
        ],

        'public' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public'),
            'visibility' => 'public',
        ],
        's3'     => [
            'driver' => 's3',
            'key'    => 'AKIAZAWAYLUIYU3JI4VL',
            'secret' => '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',
            'region' => 'us-east-1',
            'bucket' => 'council-test',
            'url'    => 'http://council-test.s3-us-east-1.amazonaws.com',
        ],
        'ftp'    => [
            'driver'   => 'ftp',
            'host'     => '112.213.89.105',
            'username' => 'fashsvnc@store.file.fashsvn.com',
            'password' => 'sangnguyen123',

            // Optional FTP Settings...
            // 'port'     => 21,
            'root'     => 'fileBackUpKDrive',
            // 'passive'  => true,
            // 'ssl'      => true,
            // 'timeout'  => 30,
        ],
//  //
    ],

];
